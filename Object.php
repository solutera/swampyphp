<?php 

/**
 *  General Object class wrapper (inspired by Objective-C :)
 *
 *  SwampyPHP Framework
 *
 *  @author	Domas Labokas <domas@solutera.lt>
 *  @copyright  Copyright (c) 2015, JSC Solutera
 *  @version	1.0
 *  @license	End User License Agreement (EULA)
 *  @link	http://www.solutera.lt
 *   
 */

namespace SwampyPHP;

function get_object_public_vars($object)
{
	return get_object_vars($object);
}

use Exception;

abstract class Object extends \stdClass implements \JsonSerializable
{
	protected function __construct(){}
	final public function __destruct()
	{
		$this->dealloc();
	}

	/**
	 * @return array
	 */
	public function jsonSerialize()
	{
		$vars = $this->getVars();
		$vars = array_map(function($o) { return ($o instanceof \JsonSerializable) ? $o->jsonSerialize() : $o; }, $vars);
		return $vars;
	}

	/**
	 * Allocate object
	 * @return static
	 */
	public static function Alloc()
	{
		return new static();
	}

	/**
	 * Deallocate object
	 */
	protected function dealloc(){}

	/**
	 * Initialize object
	 * @return static
	 */
	protected function init()
	{
		return $this;
	}

	/**
	 * Initialize object with properties
	 * @param array $vars
	 * @return static
	 */
	protected function initWithVars(array $vars)
	{
		$this->init();
		$this->setVars($vars);
		return $this;
	}

	/**
	 * Allocate & Init object
	 * @return static
	 */
	public static function Object()
	{
		return static::Alloc()->init();
	}

	/**
	 * Allocate & Init object with properties
	 * @param array $vars
	 * @return static
	 */
	public static function ObjectWithVars(array $vars)
	{
		return static::Alloc()->initWithVars($vars);
	}

	/**
	 * Set object properties
	 * @param array $vars
	 * @param null|array $filter
	 */
	public function setVars(array $vars, $filter = null)
	{
		if ($filter && !is_array($filter))
		{
			$filter = func_get_args();
			array_shift($filter);
		}

		$public_vars = get_object_public_vars($this);

		foreach ($vars as $name => $value)
		{
			if ($filter==null || in_array($name, $filter))
			{
				$method = 'set'.str_replace('_', '', $name);

				if (method_exists($this, $method))
					$this->$method($value);
				elseif (array_key_exists($name, $public_vars))
					$this->$name = $value;
			}
		}
	}

	/**
	 * Get object properties
	 * @param null|array $filter
	 * @return array
	 * @throws \Exception
	 */
	public function getVars($filter = null)
	{
		$public_vars = get_object_public_vars($this);
		$filter = is_array($filter) ? $filter : func_get_args();
		$names = count($filter) ? $filter : array_keys($public_vars);
		$vars = array();

		foreach($names as $name)
			$vars[$name] = array_key_exists($name, $public_vars) ? $public_vars[$name] : $this->__get($name);

		return $vars;
	}

	/**
	 * @param string $name
	 * @return mixed
	 * @throws Exception
	 */
	public function __get($name)
	{
		$method = 'get'.str_replace('_', '', $name);

		if (!method_exists($this, $method))
			throw new Exception('Getter method for "'.$name.'" on "'.get_class($this).'" class does not exists!');

		return $this->$method();
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @throws Exception
	 */
	public function __set($name, $value)
	{
		$method = 'set'.str_replace('_', '', $name);

		if (!method_exists($this, $method))
			throw new Exception('Setter method for "'.$name.'" on "'.get_class($this).'" class does not exists!');

		$this->$method($value);
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		$method = 'get'.str_replace('_', '', $name);
		return method_exists($this, $method);
	}
}
