<?php

/**
 *  Error Class
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\Model;

use SwampyPHP\Object;

class Error extends Object
{
	/** @var int  */
	public $code = 0;
	/** @var int  */
	public $line = 0;
	/** @var string  */
	public $file = null;
	/** @var string  */
	public $message = null;

	/**
	 * @param $code
	 * @param string|null $message
	 * @param int $line
	 * @param string|null $file
	 * @return Error
	 */
	public static function Error($code, $message = null, $line = 0, $file = null)
	{
		$error = Error::Object();

		$error->code = $code;
		$error->line = $line;		
		$error->file = $file;
		$error->message = $message;

		return $error;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->message;
	}
}
