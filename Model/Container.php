<?php

/**
 *  Container Class
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\Model;

use Iterator;
use SwampyPHP\Object;

class Container extends Object implements Iterator
{
	/** @var array  */
	private $_objects = array();
	/** @var int  */
	private $_position = 0;

	/**
	 * @param Object $model
	 */
	public function addObject(Object $model)
	{
		array_push($this->_objects, $model);
	}

	/**
	 * @param string $name
	 * @param mixed $model
	 */
	public function setObject($name, $model)
	{
		$this->_objects[$name] = $model;
	}

	/**
	 * @return int
	 */
	public function count()
	{
		return count($this->_objects);
	}

	/**
	 * @return array
	 */
	public function jsonSerialize()
	{
		$array = array_map(function ($o) { return ($o instanceof \JsonSerializable) ? $o->jsonSerialize() : $o; }, $this->_objects);

		return count($array) ? $array : array();
	}

	function rewind()
	{
		$this->_position = 0;
	}

	/**
	 * @return mixed
	 */
	function current()
	{
		return $this->_objects[$this->_position];
	}

	/**
	 * @return int
	 */
	function key()
	{
		return $this->_position;
	}

	function next()
	{
		$this->_position++;
	}

	/**
	 * @return bool
	 */
	function valid()
	{
		return isset($this->_objects[$this->_position]);
	}
}
