<?php
/**
 *  Database Model
 *
 *  Swampy-PHP Framework
 *
 *  2008 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\Database;

use Exception;
use SwampyPHP\Database as DB;
use SwampyPHP\Object;

abstract class Model extends Object
{
	/** @var array */
	protected static $identifier = array('id');
	/** @var array */
	protected static $relations = array();
//	/** @var Model */
//	protected $parent = null; //TODO: use me one day senpai t(-.-t)

	/**
	 * @return string
	 */
	public static function Table()
	{
		$class_name = get_called_class();
		$class_parts = explode("\\", $class_name);
		return strtolower(preg_replace('/\B([A-Z])/', '_$1', end($class_parts)));
	}

	/**
	 * @param mixed $id
	 * @return static
	 */
	public static function ModelWithID($id /*, $id2, ...*/)
	{
		$condition = self::_identifierCondition(func_get_args());

		if (!$condition)
			return null;

		return DB::Instance()->query("SELECT * FROM `".static::Table()."` WHERE ".$condition)->object(get_called_class());
	}

	/**
	 * Set object properties
	 * @param array $vars
	 * @param null|array $filter
	 */
	public function setVars(array $vars, $filter = null)
	{
		// Extract related values from namespace keys
		$relations = array();

		foreach ($vars as $name => $value)
		{
			$split = explode('.', $name, 2);
			if (count($split) == 2)
			{
				list($relation, $relation_name) = $split;

				if (array_key_exists($relation, static::$relations))
				{
					if (is_array($this->{$relation}))
						$relations[$relation]['.'][$relation_name] = $value;
					else
						$relations[$relation][$relation_name] = $value;

					unset($vars[$name]);
				}
			}
			elseif (is_array($value) && array_key_exists($name, static::$relations))
			{
				$relations[$name] = $value;
				unset($vars[$name]);
			}
		}

		parent::setVars($vars, $filter);

		// Set relation objects data
		foreach ($relations as $name => $value)
		{
			if (array_key_exists($name, static::$relations) && is_array($value))
			{
				if (is_array($this->{$name}))
				{
					foreach ($value as $val)
					{
						$obj = $this->childObjectWithVars($name, array_merge($val, $vars), $filter);
						$identifiers = array_diff_key($obj->identifierVars(), $this->identifierVars());
						$key = implode('-', $identifiers);

						if (count($identifiers) == 1 && empty(array_values($identifiers)[0]))
							$this->{$name}[] = $obj;
						else
							$this->{$name}[$key] = $obj;
					}
				}
				else
					$this->{$name} = $this->childObjectWithVars($name, array_merge($value, $vars), $filter);
			}
		}
	}

	/**
	 * @param string $name
	 * @param array $vars
	 * @param array $filter [optional]
	 * @return Model
	 */
	private function childObjectWithVars($name, array $vars, $filter = null)
	{
		/** @var Model $relation_class_name */
		$relation_class_name = static::$relations[$name];
		$child = $relation_class_name::Object();
		$child->setVars($vars, $filter);
		$child->setVars($this->identifierVars());
		return $child;
	}


	/**
	 * @return array
	 */
	public function identifierVars()
	{
		$vars = array();

		foreach (static::$identifier as $name)
			$vars[$name] = $this->$name;

		return $vars;
	}

	/**
	 * @return mixed
	 */
	public function getID()
	{
		$identifiers = $this->identifierVars();
		return $this->isAutoIncremental() ? intval(reset($identifiers)) : join('-', $identifiers);
	}

	/**
	 * @return bool
	 */
	public function isAutoIncremental()
	{
		return count(static::$identifier) == 1;
	}

	/**
	 * @param array $data
	 * @return array
	 */
	protected function prepareData(array $data)
	{
		return $data;
	}

	/**
	 * @param array $filter
	 * @return bool|int
	 */
	public function save($filter = array())
	{
		$r = null;
		// Get filtered data
		$data = $this->getVars(is_array($filter) ? $filter : func_get_args());

		// Unset related objects and keep references
		$related_data = array();

		foreach (static::$relations as $relation => $value)
		{
			if (isset($data[$relation]))
			{
				$related_data[$relation] = $data[$relation];
				unset($data[$relation]);
			}
		}

		// Add or Update data
		$identifier_vars = $this->identifierVars();
		$condition = count($identifier_vars) ? self::_identifierCondition($identifier_vars) : null;

		$data = $this->prepareData($data);

		if ($condition && (int)DB::Instance()->getValue("SELECT COUNT(*) FROM `".static::Table()."` WHERE ".$condition." LIMIT 1"))
			$r = $this->update($data);
		else
			$r = $this->add($data);

		// Save related data
		if ($r && count($related_data))
			foreach($related_data as $relation => $data)
				$this->saveRelation($this->$relation);

		return $r;
	}

	/**
	 * @param array|Model $obj
	 */
	protected function saveRelation($obj)
	{
		if (is_array($obj))
		{
			foreach ($obj as $o)
				$this->saveRelation($o);
		}
		else
		{
			// Pass identifier values to related(child) object
			foreach(static::$identifier as $name)
				$obj->$name = $this->$name;

			$obj->save();
		}
	}

	/**
	 * @param array $data
	 * @return int|bool
	 */
	protected function add($data)
	{
		$auto_incremental_key = null;

		if ($this->isAutoIncremental())
		{
			$auto_incremental_key = reset(static::$identifier);
			unset($data[$auto_incremental_key]);
		}

		$r = DB::Instance()->insertRowFromArray(static::Table(), $data);

		if ($auto_incremental_key)
			$this->$auto_incremental_key = (int)$r;

		return $r;
	}

	/**
	 * @param array $data
	 * @return bool
	 */
	protected function update($data)
	{
		foreach (static::$identifier as $name)
			$data[$name] = $this->$name;

		return DB::Instance()->updateFromArray(static::Table(), $data, self::_identifierCondition($this->identifierVars())) !== false;
	}

	/**
	 * @return bool
	 */
	public function delete()
	{
		$condition = self::_identifierCondition($this->identifierVars());

		if (!$condition)
			return false;

		return DB::Instance()->exec("DELETE FROM `".static::Table()."` WHERE ".$condition);
	}


	/**
	 * @param string $name
	 * @param array $arguments
	 * @return mixed
	 * @throws \Exception
	 */
	public function __call($name, $arguments)
	{
		if (!property_exists($this, $name))
			throw new Exception('Method not found: '. $name);

		if (!isset(static::$relations[$name]))
			throw new Exception('Relation not found: '. $name);

		$key = implode('-', $arguments);
		/** @var Model $class_name */
		$class_name = static::$relations[$name];

		// Load all relations eg. $text->translations();
		if (count($arguments) == 0 && is_array($this->{$name}))
		{
			if (empty($this->{$name}))
			{
				$childs = DB::Instance()->query("SELECT * FROM `" . $class_name::Table() . "` WHERE " . self::_identifierCondition($this->identifierVars()))->iterator($class_name);

				$parent_key = $this->getID();

				/** @var Model $child */
				foreach ($childs as $child) {
					$child_key = $child->getID();
					$key = $child->isAutoIncremental() ? $child_key : substr($child_key, strlen($parent_key) + 1);
					$this->{$name}[$key] = $child;
				}
			}
			return $this->{$name};
		}
		// One on One relation object
		elseif ($this->{$name} === null)
		{
			$identifierVars = array();

			foreach ($class_name::$identifier as $id)
				$identifierVars[$id] = $this->$id;

			$this->{$name} = call_user_func_array(array($class_name, 'ModelWithID'), $identifierVars);

			if (!$this->{$name})
				$this->{$name} = $class_name::Object();

			return $this->{$name};
		}
		elseif (is_array($this->{$name}) && !isset($this->{$name}[$key]))
		{
			$keys = array_reverse(static::$identifier);

			foreach ($keys as $i => $k)
				array_unshift($arguments, $this->{$k});

			$this->{$name}[$key] = call_user_func_array(array($class_name, 'ModelWithID'), $arguments);

			// Dynamically create empty Object
			if (!$this->{$name}[$key])
				$this->{$name}[$key] = $class_name::Object();

			// Set child identifier vars
			foreach ($class_name::$identifier as $i => $n)
				$this->{$name}[$key]->$n = $arguments[$i];
		}

		return is_array($this->{$name}) ? $this->{$name}[$key] : $this->{$name};
	}

	/**
	 * @param string $name
	 * @return Model
	 * @throws \Exception
	 */
	public function __get($name)
	{
		$method = 'get'.str_replace('_', '', $name);

		if (!method_exists($this, $method))
			return $this->__call($name, array());

		return $this->$method();
	}

	/**
	 * @param array $identifier_values
	 * @return string|null
	 */
	private static function _identifierCondition(array $identifier_values)
	{
		$identifier_values = array_values($identifier_values);
		$identifier_names = static::$identifier;

		if (count($identifier_values) != count($identifier_names))
			return null;

		$conditions = array();

		foreach ($identifier_names as $i => $name)
			array_push($conditions, "`".$name."`=".DB::Instance()->escape($identifier_values[$i]));

		$condition = join(' AND ', $conditions);

		return $condition;
	}
}
