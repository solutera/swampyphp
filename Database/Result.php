<?php
/**
 *  Database Result Class
 *
 *  SwampyPHP Framework
 *
 *  @author	Domas Labokas <domas@solutera.lt>
 *  @copyright  Copyright (c) 2016, UAB Solutera
 *  @version	1.0
 *  @license	End User License Agreement (EULA)
 *  @link	http://www.solutera.lt
 *
 */

namespace SwampyPHP\Database;

use PDO as PDO;

class Result implements \Iterator
{
	/** @var Statement */
	private $_statement = null;
	/** @var array */
	private $_row = null;
	/** @var string */
	private $_identifier = null;
	/** @var int */
	private $_position = 0;
	/** @var string */
	private $_ioc_name = null;

	/**
	 * Result constructor.
	 * @param Statement $statement
	 */
	public function __construct($statement)
	{
		$this->_statement = $statement;
	}

	public function rewind()
	{
		$this->_row = $this->_statement->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_FIRST);
		$this->_position = 0;
	}

	/**
	 * @return array
	 */
	public function current()
	{
		/** @var Object $class_name */
		$class_name = $this->_ioc_name;
 		return $class_name ? $class_name::ObjectWithVars($this->_row) : $this->_row;
	}

	/**
	 * @return int|mixed
	 */
	public function key()
	{
  		return $this->_identifier ? $this->_row[$this->_identifier] : $this->_position;
	}
      
	public function next() 
	{
		$this->_row = $this->_statement->fetch(PDO::FETCH_ASSOC);
		$this->_position++;	
	}

	/**
	 * @return bool
	 */
	public function valid()
	{
 	    return is_array($this->_row) ? true : false;
	}

	/**
	 * @param string $class_name
	 */
	public function setIteratorObjectClassName($class_name)
	{
		$this->_ioc_name = ($class_name && is_subclass_of($class_name, '\SwampyPHP\Object')) ? $class_name: null;
	}

	/**
	 * @return bool
	 */
	public function hasData()
	{
		return $this->_statement->rowCount() ? true : false;
	}

	/**
	 * @param string $identifier
	 */
	public function setIdentifier($identifier)
	{
		$this->_identifier = $identifier;
	}

	/**
	 * @return array
	 */
	public function row()
	{
		return $this->_statement->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @param null|string $key
	 * @return null|string
	 */
	public function value($key = null)
	{
		if ($key)
		{
			$row = $this->row();
			return isset($row[$key]) ? $row[$key] : null;
		}
		else
			return $this->_statement->fetchColumn();
	}

	/**
	 * @param null|string $key_column
	 * @param null|string $value_column
	 * @return string[]
	 */
	public function assocArray($key_column = null, $value_column = null)
	{
		$array = array();
		
		while ($row = $this->_statement->fetch(PDO::FETCH_ASSOC))
			$array[$key_column ? $row[$key_column] : current($row)] = $value_column ? $row[$value_column] : next($row);
				
		return $array;		
	}

	/**
	 * @param null|string $identifier
	 * @return string[][]
	 */
	public function table($identifier = null)
	{
		$this->_identifier = $identifier;
		$table = array();

		foreach ($this as $key => $value)
			$table[$key] = $value;

		return $table;
	}

	/**
	 * @param string $class_name
	 * @return Object|Model|null
	 */
	public function object($class_name)
	{
		/** @var Object $class_name */
		if (is_subclass_of($class_name, '\SwampyPHP\Object'))
			return ($row = $this->row()) ? $class_name::ObjectWithVars($row) : null;
		else
			return $this->_statement->fetchObject($class_name);
	}

	/**
	 * @param string $class_name
	 * @return Result
	 */
	public function iterator($class_name)
	{
		$this->setIteratorObjectClassName($class_name);
		return $this;
	}
}
