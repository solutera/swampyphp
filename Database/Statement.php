<?php

namespace SwampyPHP\Database;

use PDOStatement;
use SwampyPHP\Database;

class Statement extends PDOStatement
{
	/** @var Database  */
	private $_database = null;
	/** @var Result  */
	private $_result = null;

	/**
	 * Statement constructor.
	 * @param Database $database
	 */
	protected function __construct(Database $database)
	{
		$this->_database = $database;
	}

	/**
	 * @param array $array
	 */
	public function bindArray(array $array)
	{
		foreach ($array as $name => $value)
			$this->bindValue(is_string($name) ? ':'.$name : $name, $value);
	}

	/**
	 * @param array $input_parameters
	 * @return Result|null
	 */
	public function execute($input_parameters = null)
	{
		$this->_result = new Result($this);

		return parent::execute($input_parameters) ? $this->_result : null;
	}

	/**
	 * @return Result
	 */
	private function _result()
	{
		if (!$this->_result)
			$this->execute();

		return $this->_result;
	}

	/**
	 * @param null|string $column
	 * @return null|string
	 */
	public function fetchValue($column = null)
	{
		return $this->_result()->value($column);
	}

	/**
	 * @param null|string $key_column
	 * @param null|string $value_column
	 * @return string[]
	 */
	public function fetchArray($key_column = null, $value_column = null)
	{
		return $this->_result()->assocArray($key_column, $value_column);
	}

	/**
	 * @return array
	 */
	public function fetchRow()
	{
		return $this->_result()->row();
	}

	/**
	 * @param null|string $identifier
	 * @return string[][]
	 */
	public function fetchTable($identifier = null)
	{
		return $this->_result()->table($identifier);
	}

	/**
	 * @param array $params
	 * @return string
	 */
	public function preview(array $params)
	{
		$sql = $this->queryString;

		foreach ($params as $k => $p)
			$sql = str_replace(':'.$k, "'$p'", $sql);

		return $sql;
	}
}
