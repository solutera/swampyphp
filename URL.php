<?php

/**
 *  URL class
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link     http://www.solutera.lt
 */

namespace SwampyPHP;

class URL extends Object
{
	// URL components
	/** @var string  */
	public $scheme = 'http';
	/** @var string  */
	public $host = 'localhost';
	/** @var string  */
	public $path = '/';
	/** @var array  */
	public $params = array();

	/** @var int  */
	private $_enc_type = PHP_QUERY_RFC1738;

	/**
	 * @param int $enc_type
	 * @return string
	 */
	public function jsonSerialize($enc_type = PHP_QUERY_RFC1738)
	{
		$this->_enc_type = $enc_type;

		return $this->__toString();
	}

	/**
	 * @param string $url_string
	 * @return URL
	 */
	public function initWithString($url_string)
	{
		parent::init();

		$this->setString($url_string);

		return $this;
	}

	/**
	 * @param string $scheme
	 * @param string $host
	 * @param string $path
	 * @param array $params
	 * @return URL
	 */
	public function initWithComponents($scheme = 'http', $host = 'domain.com', $path = '/', $params = array())
	{
		parent::init();

		$this->scheme = $scheme;
		$this->host = $host;
		$this->path = $path;
		$this->params = $params;

		return $this;
	}

	/**
	 * @return URL
	 */
	public function init()
	{
		parent::init();

		$scheme = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? 'https' : 'http';
		list($path, $foo) = explode('?', $_SERVER['REQUEST_URI'].'?', 2);

		return $this->initWithComponents($scheme, $_SERVER['HTTP_HOST'], $path, $_GET);
	}

	/**
	 * @param string $url_string
	 */
	public function setString($url_string)
	{
		$components = parse_url($url_string);

		foreach (array('scheme', 'host', 'path') as $name)
			$this->$name = isset($components[$name]) ? $components[$name] : null;

		if (isset($components['query']))
			parse_str($components['query'], $this->params);
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		$string = '';

		if ($this->scheme)
			$string .= $this->scheme.':';

		if ($this->host)
			$string .= '//'.$this->host;

		$string .= $this->path;

		if ($this->params && count($this->params))
			$string .= '?'.http_build_query($this->params, $this->_enc_type);

		return $string;
	}

	/**
	 * @param string $name
	 * @param string $value
	 */
	public function setParam($name, $value)
	{
		$this->params[$name] = $value;
	}

	/**
	 * @param string $name
	 * @return string|null
	 */
	public function getParam($name)
	{
		return isset($this->params[$name]) ? $this->params[$name] : null;
	}
}
