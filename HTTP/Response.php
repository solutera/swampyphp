<?php 

/**
 *  HTTP Response class for RESTful applications
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *   
 */

namespace SwampyPHP\HTTP;

use SwampyPHP\HTTP\Header\Authorization;
use SwampyPHP\HTTP\Header\Range;

class Response extends Message
{
	/** @var int  */
	private $status_code = StatusCode::OK;
	/** @var Request  */
	private $_request = null;

	/**
	 * @param Request $request
	 * @return Response
	 */
	public function initWithIncomingRequest(Request $request)
	{
		parent::init();
		
		// Accepted content type
		if (($accept = $request->getAccept()))
			$this->setContentType(reset($accept));
		else
			$this->setContentType(ContentType::APPLICATION_JSON);
		
		// TODO - Parse set-cookie
		
		$this->_request = $request;		
		
		return $this;
	}

	/**
	 * @param Request $request
	 * @param int $status_code
	 * @param string $data
	 * @return Response
	 */
	public function initWithOutgoingRequest(Request $request, $status_code, $data)
	{
		parent::init();

		$this->_request = $request;
		
		// Set status code
		$this->status_code = $status_code;

		// Set url
		$this->url = $request->url;
		
		// Set data
		if($data)
		{
			// Separate headers from data
			list($header_data, $this->data) = explode("\r\n\r\n", $data, 2);
			
			// Parse headers
			$headers = explode("\r\n", $header_data);
			unset($headers[0]);

			foreach ($headers as $header)
			{
				list($name, $value) = explode(': ', $header, 2);
				$this->setHeader($name, $value);
			}
		}
		
		return $this;
	}

	/**
	 * Send Headers
	 */
	private function sendHeaders()
	{
		// Set response code
		header('HTTP/1.1 '.$this->status_code.' '.StatusCode::Description($this->status_code), true, $this->status_code);
	        
		// Set headers
		foreach ($this->headers as $header => $value)
				header($header.': '.$value, true);

		// TODO Set cookie
	}

	/**
	 * Send Data
	 */
	public function send()
	{
		// Return data
		if ($this->data !== null)
		{
			switch($this->getContentType())
			{
				case ContentType::APPLICATION_FILE:
				case ContentType::IMAGE:
				case ContentType::IMAGE_GIF:
				case ContentType::IMAGE_PNG:
				case ContentType::IMAGE_JPG:
				case ContentType::IMAGE_JPEG:
					$this->sendFile($this->data);		
					break;		
				default:
					$this->sendHeaders();
					echo $this->data;
					break;
			}
		}
		else
			$this->sendHeaders();
	}

	/**
	 * Send File
	 * @param string|null $path
	 */
	public function sendFile($path = null)
	{
		$path = $path ? $path : $this->data;
		$fd = (gettype($path) == 'resource') ? $path : (file_exists($path) ? @fopen($path, "r") : null);
	
		if ($fd)
		{
			// Get file info for mime extraction
			$finfo = new \finfo(FILEINFO_MIME);
			$buffer = fread($fd, 32);
			$mime = $finfo->buffer($buffer);

			//$meta = stream_get_meta_data($fd);

			// Get file stats
			$stat = fstat($fd);
			$last_modified = ($stat[9] > 0) ? $stat[9] : null; //filemtime($path);
			$file_size = $stat[7];

			// Check last modification
			if ($last_modified)
			{
				$mod_since = $this->_request ? $this->_request->getIfModifiedSince() : null;

				if($mod_since && $mod_since >= $last_modified)
				{
					//header('Etag: '. md5(join(':', $stat)));
					header('HTTP/1.1 304 Not Modified', true, 304);
					exit;
				}

				$this->setHeader('Last-Modified', gmdate('D, d M Y H:i:s', $last_modified) . ' GMT');
			}

			// Set headers
			$this->setHeader(HeaderField::PRAGMA, 'public');
			$this->setHeader(HeaderField::CONTENT_LENGTH, $file_size);
			$this->setHeader(HeaderField::CONTENT_TYPE, $mime);

			// Send headers
			$this->sendHeaders();

			rewind($fd);
			fpassthru($fd);
			fclose($fd);			
			exit;
		}
		else
		{
			header('HTTP/1.1 404 Not Found', true, 404);
			exit;
		}
	}

	/**
	 * Set Auth header
	 * @param Authorization $auth
	 */
	public function setWWWAuthenticate(Authorization $auth)
	{
		$params = null;
		
		if ($auth->type == Authorization::TYPE_DIGEST)
		{
			$nonce = $auth->nonce ? $auth->nonce : md5(uniqid());
			$params = sprintf('realm="%s", nonce="%s", opaque="%s"', $auth->realm, $nonce, $auth->opaque);
		}
		else
			$params = sprintf('realm="%s"', $auth->realm);
	
		$this->setHeader(HeaderField::WWW_AUTHENTICATE, $auth->type.' '.$params);
	}

	/**
	 * Get Auth header
	 * @return Authorization|null
	 */
	public function getWWWAuthenticate()
	{
		$auth_header = $this->getHeader(HeaderField::WWW_AUTHENTICATE);

		if ($auth_header)
		{
			$auth = Authorization::Alloc()->initWithHeader($auth_header);
			$auth->method = strtoupper($this->method);

			return $auth;
		}

		return null;
	}

	/**
	 * @param int $code
	 */
	public function setStatusCode($code)
	{
		$this->status_code = (int) $code;
	}

	/**
	 * @return int
	 */
	public function getStatusCode()
	{
		return $this->status_code;
	}

	/**
	 * Set Content-Language header
	 * @param string $language
	 */
	public function setContentLanguage($language)
	{
		$this->setHeader(HeaderField::CONTENT_LANGUAGE, $language);
	}

	/**
	 * @param Range|array $range
	 */
	public function setContentRange(Range $range)
	{
		$range = is_array($range) ? implode(', ', $range) : (string)$range;
		$this->setHeader(HeaderField::CONTENT_RANGE, $range);
	}

	/**
	 * @return int
	 */
	public function getLastModified()
	{
		$last_modified = $this->getHeader(HeaderField::LAST_MODIFIED);

		return $last_modified ? strtotime($last_modified) : 0;
	}

	/**
	 * @param int $last_modified_timestamp
	 */
	public function setLastModified($last_modified_timestamp)
	{
		$this->setHeader(HeaderField::LAST_MODIFIED, gmdate('D, d M Y H:i:s T', $last_modified_timestamp));
	}
}
