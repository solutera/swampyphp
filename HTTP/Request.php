<?php 

/**
 *  HTTP Request class for RESTful applications
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *   
 */

namespace SwampyPHP\HTTP;

use Exception;
use SwampyPHP\HTTP\Header\Authorization;
use SwampyPHP\HTTP\Header\Range;
use SwampyPHP\URL;

class Request extends Message
{
	/** @var Connection  */
	protected $connection = null;

	/**
	 * @param Connection $connection
	 * @return Request
	 */
	public function initWithConnection(Connection $connection)
	{
		parent::init();

		$this->connection = $connection;

		// Get method
		$this->method = strtoupper($_SERVER['REQUEST_METHOD']);

		// Get url components
		$this->url = URL::Object();

		// Get headers
		$this->setHeaders();

		// Get data
		$this->setRawData(file_get_contents('php://input'));

		return $this;
	}

	/**
	 * @return Response
	 * @throws Exception
	 */
	public function send()
	{
		// Setup curl request
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, (string)$this->url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, true);
		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $this->data);

		// Set content length
		$this->setContentLength(strlen($this->data));

		// Continue fix
		$this->setHeader(HeaderField::EXPECT, '');

		// Set headers
		if ($this->headers && count($this->headers))
		{
			$headers = array();
			foreach($this->headers as $name => $value)
				array_push($headers, $name.': '.$value);

			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		}

		// Send request
		$data = curl_exec($curl);

		// Get error if occurred
		if ($data === false)
		{
			$error_message = curl_error($curl);
			$error_code = curl_errno($curl);
			curl_close($curl);
			throw new Exception($error_message, $error_code);
		}

		// Generate response
		$response_status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$response = Response::Alloc()->initWithOutgoingRequest($this, $response_status_code, $data);

		// Close connection
		curl_close($curl);

		// Return response
		return $response;
	}

	/**
	 * Set Authorization header
	 * @param Authorization $auth
	 */
	public function setAuthorization(Authorization $auth)
	{
		$this->setHeader(HeaderField::AUTHORIZATION, (string) $auth);
	}

	/**
	 * Get authorization header
	 * @return Authorization|null
	 */
	public function getAuthorization()
	{
		$auth_header = $this->getHeader(HeaderField::AUTHORIZATION);

		return $auth_header ? Authorization::Alloc()->initWithHeader($auth_header) : null;
	}

	/**
	 * @param int $length
	 */
	public function setContentLength($length)
	{
		$this->setHeader(HeaderField::CONTENT_LENGTH, $length);
	}

	/**
	 * @param string|array $content_type
	 */
	public function setAccept($content_type)
	{
		$content_type = implode(',', is_array($content_type) ? $content_type : func_get_args());
		$this->setHeader(HeaderField::ACCEPT, $content_type);
	}

	/**
	 * @return array|null
	 */
	public function getAccept()
	{
		$content_types = $this->getHeader(HeaderField::ACCEPT);

		return $content_types ? explode(',', $content_types) : null;
	}

	/**
	 * @param string $language
	 */
	public function setAcceptLanguage($language)
	{
		$this->setHeader(HeaderField::ACCEPT_LANGUAGE, $language);
	}

	/**
	 * @param string|null $default
	 * @return string|null
	 */
	public function getAcceptLanguage($default = null)
	{
		$lang = $this->getHeader(HeaderField::ACCEPT_LANGUAGE);

		return $lang ? substr($lang, 0, 2) : $default;
	}

	/**
	 * @param string|null $name
	 * @return Range|array|null
	 */
	public function getRange($name = null)
	{
		$ranges = array();
		$range = $this->getHeader(HeaderField::RANGE);

		if ($range) {
			$list = explode(',', $range);

			foreach ($list as $value)
			{
				if (($range = Range::Alloc()->initWithString($value)))
					$ranges[$range->name] = $range;
			}

			return ($name === null) ? $ranges : (isset($ranges[$name]) ? $ranges[$name] : null);
		}

		return null;
	}

	/**
	 * @param Range $range
	 */
	public function setRange(Range $range)
	{
		$this->setHeader(HeaderField::RANGE, (string) $range);
	}

	/**
	 * @param int $default_timestamp
	 * @return int
	 */
	public function getIfModifiedSince($default_timestamp = 0)
	{
		$mod_since = $this->getHeader(HeaderField::IF_MODIFIED_SINCE);

		return $mod_since ? strtotime($mod_since) : $default_timestamp;
	}

	/**
	 * @param int $timestamp
	 */
	public function setIfModifiedSince($timestamp)
	{
		$this->setHeader(HeaderField::IF_MODIFIED_SINCE, gmdate('D, d M Y H:i:s T', $timestamp));
	}
}
