<?php

/**
 *  Cookie Class
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.1
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\HTTP;

class Exception extends \Exception
{
	/**
	 * Exception constructor.
	 * @param string $code
	 */
	public function __construct($code)
	{
		parent::__construct(StatusCode::Description($code), $code);
	}
}
