<?php

/**
 *  Cookie Class
 *
 *  Swampy-PHP Framework
 *
 *  2008 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.1
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\HTTP;

use stdClass;

class Cookie extends stdClass
{
	const EXPIRE_IN_SESSION = 0;
	const EXPIRE_IN_DAY = 86400;
	const EXPIRE_IN_WEEK = 604800;
	const EXPIRE_IN_MONTH = 2592000;
	const EXPIRE_IN_YEAR = 31536000;

	/** @var array|mixed  */
	private $_content = array();
	/** @var string  */
	private $_name = null;
	/** @var null|string  */
	private $_base_url = null;

	/**
	 * Cookie constructor.
	 * @param string $name
	 * @param string $base_url
	 */
	public function __construct($name, $base_url = '/')
	{
		$this->_name = $name;
		$this->_base_url = $base_url;
		$this->_content = json_decode(self::Get($this->_name, '[]'), true);
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set($name, $value)
	{
		$this->_content[$name] = $value;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		return $this->_content[$name];
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		return isset($this->_content[$name]);
	}

	/**
	 * @param string $name
	 */
	public function __unset($name)
	{
		unset($this->_content[$name]);		
	}

	/**
	 * @return bool
	 */
	public function save()
	{
		return self::Set($this->_name, json_encode($this->_content), self::EXPIRE_IN_MONTH);
	}

	/**
	 * @return bool
	 */
	public function delete()
	{
		return self::Remove($this->_name, $this->_base_url);
	}

	/**
	 * @param string $name
	 * @param mixed $default
	 * @return mixed
	 */
	public static function Get($name, $default = null)
	{
		return (isset($_COOKIE[$name]) ? $_COOKIE[$name] : $default);
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public static function Exists($name)
	{
		return isset($_COOKIE[$name]);
	}

	/**
	 * @param string $name
	 * @param string $value
	 * @param int $expiry
	 * @param string $path
	 * @param string $domain
	 * @return bool
	 */
	public static function Set($name, $value, $expiry = self::EXPIRE_IN_MONTH, $path = '/', $domain = null)
	{
		$r = false;

		if (!headers_sent())
		{
			$expiry = ($expiry == self::EXPIRE_IN_SESSION) ? 0 : (is_numeric($expiry) ? $expiry + time() : strtotime($expiry));

			if (($r = setrawcookie($name, rawurlencode($value), $expiry, $path, $domain)))
				$_COOKIE[$name] = $value;
		}

		return $r;
	}

	/**
	 * @param string $name
	 * @param string $path
	 * @param string $domain
	 * @return bool
	 */
	public static function Remove($name, $path = '/', $domain = null)
	{
		$retval = false;

		if (!headers_sent())
		{
			if (($retval = self::Set($name, '', time() - 3600, $path, $domain)))
				unset($_COOKIE[$name]);
		}

		return $retval;
	}
}
