<?php

/**
 *  Connection Class
 *
 *  SwampyPHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\HTTP;

use SwampyPHP\Object as Object;

abstract class Connection extends Object
{
	/** @var array  */
    protected $errors = array();

    public function __invoke()
    {
        $this->invoke();
    }

    public function invoke()
    {
        $request = Request::Alloc()->initWithConnection($this);
        $response = $this->handleRequest($request);

        if ($response)
            $response->send();
    }

	/**
	 * @param Request $request
	 * @return Response
	 */
	public function sendRequest(Request $request)
    {
        return $request->send();
    }

    /**
     * Handle incoming request
     * @param Request $request
     * @return Response
     */
    abstract protected function handleRequest(Request $request);
}
