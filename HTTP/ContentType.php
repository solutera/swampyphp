<?php

/**
 *  Content Type Class
 *
 *  SwampyPHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\HTTP;

class ContentType
{
	const TEXT_PLAIN = 			'text/plain';
	const TEXT_HTML = 			'text/html';
	const TEXT_CSV = 			'text/csv';
	const APPLICATION_JSON =	'application/json';
	const APPLICATION_FORM =	'application/x-www-form-urlencoded';
	const APPLICATION_FILE =	'application/octet-stream';
	const IMAGE =				'image/*';
	const IMAGE_GIF =			'image/gif';
	const IMAGE_PNG =			'image/png';
	const IMAGE_JPG =			'image/jpg';
	const IMAGE_JPEG =			'image/jpeg';
}
