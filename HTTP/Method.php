<?php

/**
 *  Method Class
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.1
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\HTTP;

class Method
{
	const OPTIONS = 'OPTIONS';
	const GET = 	'GET';
	const HEAD =	'HEAD';
	const POST =	'POST';
	const PUT =		'PUT';
	const DELETE =	'DELETE';
	const TRACE = 	'TRACE';
	const CONNECT = 'CONNECT';
}
