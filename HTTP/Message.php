<?php 

/**
 *  General HTTP Message class for RESTful applications
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *   
 */

namespace SwampyPHP\HTTP;

use SwampyPHP\Object;
use SwampyPHP\URL;

if (!function_exists('getallheaders'))
{
	/**
	 * @return array
	 */
	function getallheaders()
	{
		$headers = array();

		foreach ($_SERVER as $name => $value)
		{
			if (substr($name, 0, 5) == 'HTTP_')
			{
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}

		return $headers;
	}
}

class Message extends Object
{
	/** @var URL  */
	protected $url = null;
	/** @var array */
	protected $headers = array();

	/** @var Cookie  */
	protected $cookie = null;
	/** @var mixed  */
	protected $data = null;
	/** @var string  */
	protected $method = Method::GET;

	/**
	 * @param URL|string $url
	 */
	public function setURL($url)
	{
		$this->url = is_object($url) ? $url : URL::Alloc()->initWithString($url);
	}

	/**
	 * @return URL
	 */
	public function getURL()
	{
		return $this->url;
	}

	/**
	 * @param string $name
	 * @param string $value
	 */
	public function setHeader($name, $value)
	{
		$this->headers[strtolower($name)] = (string)$value;
	}

	public function setHeaders()
	{
		foreach (getallheaders() as $name => $value)
			$this->headers[strtolower($name)] = $value;
	}

	public function getHeaders()
	{
		return $this->headers;
	}
	/**
	 * @param string $name
	 * @return null|string
	 */
	public function getHeader($name)
	{
		return array_key_exists(strtolower($name), $this->headers) ? $this->headers[strtolower($name)] : null;
	}

	/**
	 * @param string $content_type
	 * @param string $charset
	 */
	public function setContentType($content_type, $charset = 'UTF-8')
	{
		$this->setHeader(HeaderField::CONTENT_TYPE, $content_type.($charset ? '; charset='.$charset : ''));
	}

	/**
	 * @return string
	 */
	public function getContentType()
	{
		$header_content_type = $this->getHeader(HeaderField::CONTENT_TYPE);
		list($content_type) = explode(';', $header_content_type.'; ', 2);

		return trim($content_type);
	}

	/**
	 * @param Cookie $cookie
	 */
	public function setCookie(Cookie $cookie)
	{
		$this->cookie = $cookie;
	}

	/**
	 * @return Cookie
	 */
	public function getCookie()
	{
		return $this->cookie;
	}

	/**
	 * @param string $method
	 */
	public function setMethod($method)
	{
		$this->method = $method;
	}

	/**
	 * @return string
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * @param mixed $data
	 * @param string|null $content_type
	 */
	public function setData($data, $content_type = null)
	{
		if ($content_type === null)
			$content_type = $this->getContentType();
		else
			$this->setContentType($content_type);

		switch ($content_type)
		{
			case ContentType::APPLICATION_JSON:
				$this->data = json_encode($data, JSON_PRETTY_PRINT|JSON_NUMERIC_CHECK);
				break;
			case ContentType::APPLICATION_FORM:
				$this->data = http_build_query($data);
				break;
			default:
				$this->data = is_string($data) ? $data : serialize($data);
				break;
		}
	}

	/**
	 * @param string $data
	 * @param int $json_flags
	 */
	public function setJsonData($data, $json_flags = JSON_NUMERIC_CHECK)
	{
		$this->setContentType(ContentType::APPLICATION_JSON);
		$this->data = json_encode($data, $json_flags);
	}

	/**
	 * @param bool $assoc
	 * @return mixed|null
	 */
	public function getData($assoc = true)
	{
		$content_type = $this->getContentType();
		$data = null;

		switch ($content_type)
		{
			case ContentType::APPLICATION_JSON:
				$data = json_decode($this->data, $assoc);
				break;
			case ContentType::APPLICATION_FORM:
				parse_str($this->data, $data);
				break;
			default:
				$this->data = is_string($data) ? $data : serialize($data);
				break;
		}

		return $data;
	}

	/**
	 * @param mixed $data
	 */
	public function setRawData($data)
	{
		$this->data = $data;
	}

	/**
	 * @return mixed
	 */
	public function getRawData()
	{
		return $this->data;
	}
}
