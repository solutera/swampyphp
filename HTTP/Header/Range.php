<?php

/**
 *  Range Header Class
 *
 *  SwampyPHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *   
 */

namespace SwampyPHP\HTTP\Header;

use SwampyPHP\Object;

final class Range extends Object
{
	/** @var string  */
	protected $name = 'items';
	/** @var int  */
	protected $first = 0;
	/** @var int  */
	protected $last = 99;
	/** @var int|null  */
	protected $length = null;

	/**
	 * @param string $name
	 * @param int $limit
	 * @param int $offset
	 * @return Range
	 */
	public static function RangeWithLimit($name, $limit = 100, $offset = 0)
	{
		return self::Alloc()->initWithLimit($name, $limit, $offset);
	}

	/**
	 * @param string $name
	 * @param int $limit
	 * @param int $offset
	 * @return Range
	 */
	public function initWithLimit($name, $limit = 100, $offset = 0)
	{
		parent::init();

		$this->name = $name;
		$this->setOffset($offset);
		$this->setLimit($limit);

		return $this;
	}

	/**
	 * @param $string
	 * @return Range|null
	 */
	public function initWithString($string)
	{
		parent::init();
		
		$matches = array();
		
		if (preg_match('/([a-z]+)\=([0-9]+)\-([0-9]+)/', $string, $matches) && count($matches) == 4)
			list($foo, $this->name, $this->first, $this->last) = $matches;
		elseif (preg_match('/([a-z]+) ([0-9]+)\-([0-9]+)\/([0-9]+)/', $string, $matches) && count($matches) == 5)
			list($foo, $this->name, $this->first, $this->last, $this->length) = $matches;
	
		if ($this->last < $this->first)
		{
			$this->dealloc();
			return null;
		}

		return $this;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{ 
		return $this->length !== null ?
			$this->name.' '.$this->first.'-'.$this->last.'/'.$this->length :
			$this->name.'='.$this->first.'-'.$this->last;
	}

	/**
	 * @return int
	 */
	public function getLimit()
	{
		return $this->last - $this->first + 1;
	}

	/**
	 * @param int $limit
	 */
	public function setLimit($limit)
	{
		$this->last = $this->first + $limit - 1;
	}

	/**
	 * @return int
	 */
	public function getOffset()
	{
		return $this->first;
	}

	/**
	 * @param int $offset
	 */
	public function setOffset($offset)
	{
		$this->last -= $this->first - $offset;
		$this->first = $offset;		
	}

	/**
	 * @param int $length
	 */
	public function setLength($length)
	{
		$this->length = $length;
		$this->last = $length > 0 ? min($this->last, $length-1) : 0;
	}

	/**
	 * @return int|null
	 */
	public function getTotal()
	{
		return $this->length;
	}

	/**
	 * @param int $total
	 */
	public function setTotal($total)
	{
		$this->setLength($total);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getFirst()
	{
		return $this->first;
	}

	/**
	 * @return int
	 */
	public function getLast()
	{
		return $this->last;
	}
}
