<?php
/**
 *  Cookie Class
 *
 *  SwampyPHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\HTTP\Header;

use SwampyPHP\Object;

final class Cookie extends Object
{
	/** @var string  */
	protected $name = null;
	/** @var string  */
	protected $value = null;
	/** @var array  */
	protected $options = array();

	/**
	 * @param string $header_string
	 */
	public function initWithHeader(string $header_string) {}

	/**
	 * @param string $name
	 * @param string $value
	 * @param array $options
	 * @return Cookie
	 */
	public function initWithNameValueOptions(string $name, string $value, array $options = array())
	{
		parent::init();

		$this->name = $name;
		$this->value = $value;
		$this->options = $options;
		
		return $this;
	}

	/**
	 * @param string|null $name
	 * @return Cookie
	 */
	public function initWithName(string $name = null)
	{
		parent::init();
		
		$this->name = $name;
		$this->value = $_COOKIE[$name];

		return $this;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function __get(string $name)
	{
		return $this->options[$name];
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set(string $name, $value)
	{
		$this->options[$name] = $value;
	}

	/**
	 * @param string $name
	 */
	public function __unset(string $name)
	{
		unset($this->options[$name]);
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		$string = $this->name.'='.urlencode($this->value);
		
		$options = array('expires', 'path', 'domain');

		foreach ($options as $opt)
		{
			if (isset($this->options[$opt]))
				$string .= '; '.$opt.'='.$this->options[$opt];
		}

		if (isset($this->options['secure']) && $this->options['secure'] === true)
			$string .= '; secure';

		return $string;
	}
}
