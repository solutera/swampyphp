<?php
/**
 *  Authorization Class
 *
 *  SwampyPHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\HTTP\Header;

use SwampyPHP\Object;

final class Authorization extends Object
{
	const TYPE_BASIC =	'Basic';
	const TYPE_DIGEST =	'Digest';
	const TYPE_TOKEN =	'Token';
	const TYPE_BEARER =	'Bearer';

	/** @var string  */
	public $method = 'GET';
	/** @var string  */
	public $type = self::TYPE_BASIC;
	/** @var string  */
	public $username = null;
	/** @var string  */
	public $password = null;
	/** @var string  */
	public $realm = null;
	/** @var string  */
	public $uri = null;
	/** @var string  */
	public $nonce = null;
	/** @var string  */
	public $opaque = null;
	/** @var string  */
	public $qop = null;
	/** @var string  */
	public $cnonce = null;
	/** @var string  */
	public $nc = null;
	/** @var string  */
	public $response = null;
	/** @var string  */
	public $token = null;

	/**
	 * @param string $username
	 * @param string $password
	 * @param string $type
	 * @return Authorization
	 */
	public function initWithCredentials($username, $password, $type = self::TYPE_BASIC)
	{
		parent::init();
		
		$this->type = $type;
		$this->username = $username;
		$this->password = $password;		
		
		return $this;
	}

	/**
	 * @param string $header_string
	 * @return Authorization
	 */
	public function initWithHeader($header_string)
	{
		parent::init();
		
		// Validate header
		$matches = array();

		if (!$header_string || !preg_match('/(Basic|Digest|Token|Bearer) (.*)/', $header_string, $matches))
			return $this;
	
		// Extract data
		list($n, $this->type, $params) = $matches;

		if ($this->type == self::TYPE_BASIC)
		{
			$credentials = base64_decode($params);
			list($this->username, $this->password) = explode(':', $credentials);
		}
		elseif ($this->type == self::TYPE_DIGEST && preg_match_all('@(realm|username|nonce|uri|nc|cnonce|qop|response|opaque)=[\'"]?([^\'",]+)@', $params, $matches))
		{
			$digest = array_combine($matches[1], $matches[2]);

			foreach ($digest as $name => $value)
				$this->$name = $value;
		}
		elseif ($this->type == self::TYPE_TOKEN && preg_match_all('@(realm|token)=[\'"]?([^\'",]+)@', $params, $matches))
		{
			$token = array_combine($matches[1], $matches[2]);

			foreach ($token as $name => $value)
				$this->$name = $value;
		}
		elseif ($this->type == self::TYPE_BEARER && $params)
		{
			$this->token = $params;
		}
		
		return $this;
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @return bool
	 */
	public function validateCredentials($username, $password)
	{
		if ($this->type == self::TYPE_BASIC && $this->username === $username && $this->password === $password)
			return true;
		elseif ($this->type == self::TYPE_DIGEST && $this->response && (string)$this->username === (string)$username)
		{
			$this->password = $password;
			$response = $this->digestResponse();

			return ($this->response === $response);
		}
		else
			return false;
	}

	/**
	 * @param string $token
	 * @return bool
	 */
	public function validateToken($token)
	{
		return (in_array($this->type, array(self::TYPE_TOKEN, self::TYPE_BEARER)) && $this->token == $token);
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		if ($this->type == self::TYPE_BASIC && $this->username && $this->password)
		{
			$credentials = $this->username.':'.$this->password;

			return $this->type . ' ' . base64_encode($credentials);
		}
		elseif ($this->type == self::TYPE_DIGEST && $this->username && $this->password && $this->realm && $this->method && $this->uri && $this->nonce && $this->opaque)
		{
			if(!$this->nonce)
				$this->nonce = md5(uniqid().time());

			$this->response = $this->digestResponse();
			$credentials = sprintf('username="%s", realm="%s", nonce="%s", opaque="%s", uri="%s", response="%s"', $this->username, $this->realm, $this->nonce, $this->opaque, $this->uri, $this->response);

			return $this->type.' '.$credentials;
		}
		elseif ($this->type == self::TYPE_TOKEN && $this->token)
		{
			$params = 'token="'.$this->token.'"';

			if($this->realm)
				$params .= ', realm="'.$this->realm.'"';

			return $this->type.' '.$params;
		}
		elseif ($this->type == self::TYPE_BEARER)
		{
			return $this->type.' '.$this->token;
		}
		
		return '';
	}

	/**
	 * @return string
	 */
	private function digestResponse()
	{
		$A1 = self::MD5ColonImplode($this->username, $this->realm, $this->password);
		$A2 = self::MD5ColonImplode(strtoupper($this->method), $this->uri);
		
		$response = ($this->qop && $this->nc && $this->cnonce) ? 
			self::MD5ColonImplode($A1, $this->nonce, $this->nc, $this->cnonce, $this->qop, $A2) : 
			self::MD5ColonImplode($A1, $this->nonce, $A2);
		
		return $response;
	}

	/**
	 * @return string
	 */
	private static function MD5ColonImplode(/* string1, string2, stringN... */)
	{
	    return md5(implode(':', func_get_args()));
	}
}
