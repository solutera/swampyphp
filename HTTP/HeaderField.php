<?php

/**
 *  Header Filed Class
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.1
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\HTTP;

class HeaderField
{
	const ACCEPT = 				'Accept';
	const ALLOW = 				'Allow';
	const ACCEPT_LANGUAGE =		'Accept-Language';
	const ACCEPT_DATETIME =		'Accept-Datetime';
	const AUTHORIZATION =		'Authorization';
	const CONTENT_TYPE = 		'Content-Type';
	const CONTENT_LANGUAGE =	'Content-Language';
	const CONTENT_RANGE = 		'Content-Range';
	const CONTENT_LENGTH =		'Content-Length';
	const CONTENT_LOCATION =	'Content-Location';
	const RANGE =				'Range';
	const IF_MODIFIED_SINCE=	'If-Modified-Since';
	const LOCATION = 			'Location';
	const WWW_AUTHENTICATE = 	'WWW-Authenticate';
	const LAST_MODIFIED = 		'Last-Modified';
	const PRAGMA = 				'Pragma';
	const EXPECT = 				'Expect';
}
