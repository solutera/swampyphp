<?php

/**
 *  Status Code Class
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\HTTP;

class StatusCode
{
	const CONTINUE_ =				100;
	const OK =						200;
	const CREATED =					201;
	const NO_CONTENT =				204;
	const NOT_MODIFIED =			304;
	const TEMPORARY_REDIRECT =		307;
	const BAD_REQUEST =				400;
	const UNAUTHORISED =			401;
	const FORBIDDEN =				403;
	const NOT_FOUND =				404;
	const METHOD_NOT_ALLOWED =		405;
	const NOT_ACCEPTABLE =			406;
	const REQUEST_TIMEOUT =			408;
	const CONFLICT =				409;
	const EXPECTATION_FAILED =		417;
	const INTERNAL_SERVER_ERROR =	500;
	const NOT_IMPLEMENTED =			501;
	const SERVICE_UNAVAILABLE =		503;

	/** @var array  */
	private static $status_code_names = array
	(
	    0 => 'Unknown',
	    100 => 'Continue',
	    101 => 'Switching Protocols',
	    200 => 'OK',
	    201 => 'Created',
	    202 => 'Accepted',
	    203 => 'Non-Authoritative Information',
	    204 => 'No Content',
	    205 => 'Reset Content',
	    206 => 'Partial Content',
	    300 => 'Multiple Choices',
	    301 => 'Moved Permanently',
	    302 => 'Found',
	    303 => 'See Other',
	    304 => 'Not Modified',
	    305 => 'Use Proxy',
	    306 => '(Unused)',
	    307 => 'Temporary Redirect',
	    400 => 'Bad Request',
	    401 => 'Unauthorised',
	    402 => 'Payment Required',
	    403 => 'Forbidden',
	    404 => 'Not Found',
	    405 => 'Method Not Allowed',
	    406 => 'Not Acceptable',
	    407 => 'Proxy Authentication Required',
	    408 => 'Request Timeout',
	    409 => 'Conflict',
	    410 => 'Gone',
	    411 => 'Length Required',
	    412 => 'Precondition Failed',
	    413 => 'Request Entity Too Large',
	    414 => 'Request-URI Too Long',
	    415 => 'Unsupported Media Type',
	    416 => 'Requested Range Not Satisfiable',
	    417 => 'Expectation Failed',
		418 => 'I\'m a teapot',
		419 => 'Authentication Timeout',
	    500 => 'Internal Server Error',
	    501 => 'Not Implemented',
	    502 => 'Bad Gateway',
	    503 => 'Service Unavailable',
	    504 => 'Gateway Timeout',
	    505 => 'HTTP Version Not Supported'
	);

	/**
	 * @param int $code
	 * @return string|null
	 */
	public static function Description($code)
	{
		return array_key_exists($code, self::$status_code_names) ? self::$status_code_names[$code] : null;
	}
}
