<?php
/**
 *  Database Class
 *
 *  SwampyPHP Framework
 *
 *  @author	Domas Labokas <domas@solutera.lt>
 *  @copyright  Copyright (c) 2016, UAB Solutera
 *  @version	1.0
 *  @license	End User License Agreement (EULA)
 *  @link	http://www.solutera.lt
 *
 */

namespace SwampyPHP;

use Exception;
use PDO;
use SwampyPHP\Database\Result;

class Database
{
	const INSTANCE_PRIMARY = 'PRIMARY';

	/** @var Database[]  */
	private static $_INSTANCE = array();
	/** @var float */
	private $_queryTime = 0;
	/** @var float */
	private $_totalTime = 0;
	/** @var bool */
	private $_debug = false;
	/** @var PDO */
	private $_db = null;

	/**
	 * @param string $name
	 * @return Database
	 */
	public static function Instance($name = self::INSTANCE_PRIMARY)
	{
		if (!isset(self::$_INSTANCE[$name]))
			self::$_INSTANCE[$name] = new self();

		return self::$_INSTANCE[$name];
	}

	/**
	 * Connect with user to specified database
	 * @param string $user
	 * @param string $pass
	 * @param string $name
	 * @param string $host
	 * @param string $charset
	 * @return bool
	 */
	public function connect($user = '', $pass = '', $name = '', $host = 'localhost', $charset = 'utf8')
	{
		try
		{
			$this->_db = new PDO("mysql:host=".$host.";dbname=".$name.';charset='.$charset, $user, $pass);
		}
		catch (Exception $e)
		{

		}
		return $this->_db ? true : false;
	}

	/**
	 * Close database connection
	 */
	public function disconnect()
	{
		$this->_db = null;
	}

	/**
	 * Check connection status
	 * @return bool
	 */
	public function connected()
	{
		return $this->_db ? true : false;
	}

	/**
	 * Get last occurred error
	 * @return null|string
	 */
	public function lastError()
	{
		$error = $this->_db->errorInfo();
		return $error ? $error[2] : null;
	}

	/**
	 * @param string[]|string $string
	 * @return string[]|string|null
	 */
	public function escape($string)
	{
		if (is_null($string))
			return null;
		elseif (!is_array($string))
			return $this->_db->quote($string);

		$strings = array();
		foreach ($string as $key=>$value)
			$strings[$this->escape($key)] = $this->escape($value);

		return $strings;
	}

	/**
	 * @param string $query
	 * @param mixed $args [optional]
	 * @param mixed $_ [optional]
	 * @return mixed|string
	 */
	public function escapeQuery($query, $args = null, $_ = null)
	{
		if (func_num_args() == 1) return func_get_arg(0);
		$args = func_get_args();
		$query = array_shift($args);
		$args = $this->_prepare($args, 1);

		$query = preg_replace("/\'%([a-zA-Z])\'/", "%$1", $query);	// Remove quotes from variables
		$query = vsprintf($query, $args);				// Generate query
		$query = preg_replace("/`\'([a-zA-Z_]+)\'`/", "`$1`", $query);	// Remove quotes from column names

		return $query;
	}

	/**
	 * @param mixed $value
	 * @param int $deep
	 * @return array|int|string
	 */
	private function _prepare($value, $deep = 0)
	{
		if (!is_array($value))
		{
			if (is_null($value))
				return "NULL";
			elseif (ctype_digit((string) $value))
				return $value;
			elseif (is_bool($value))
				return $value ? 1 : 0;
			else
				return $this->escape($value);
		}
		elseif (!$deep)
 			return implode(',', array_map(array($this, '_prepare'), $value));

		$values = array();
		foreach ($value as $column => $val)
			$values["`".trim($this->escape($column), "'")."`"] = $this->_prepare($val, $deep - 1);

		return $values;
	}

	/**
	 * Save debug start time
	 */
	private function debugBegin()
	{
		if ($this->_debug)
			$this->_queryTime = microtime(true);
	}

	/**
	 * Log debug message & calculate exec time
	 * @param string $query
	 * @param bool $hasError
	 */
	private function debugEnd($query, $hasError)
	{
		if ($this->_debug)
		{
			$t = (microtime(true) - $this->_queryTime);
			$this->_totalTime += $t;

			Log::Trace("DB Query: (%s) - Time: %0.4f sec.", $query, $t);

			if ($hasError)
				Log::Error("Query: (%s) Error: %s", $query, $this->lastError());
		}
	}

	/**
	 * @param $query
	 * @param null $args [optional]
	 * @param null $_ [optional]
	 * @return int
	 */
	public function exec($query, $args = null, $_ = null)
	{
		$statement  = call_user_func_array(array($this, 'escapeQuery'), func_get_args());
		$this->debugBegin();
		$r = $this->_db->exec($statement);
		$this->debugEnd($statement, $r === false);
		return $r;
	}

	/**
	 * @return \SwampyPHP\Database\Result|null
	 */
	public function query(/*query [, $arg1...$argN]*/)
	{
		$statement  = call_user_func_array(array($this, 'escapeQuery'), func_get_args());
		$this->debugBegin();
		/** @var \SwampyPHP\Database\Statement $r */
		$r = $this->_db->query($statement);
		$this->debugEnd($statement, $r === false);
		return  $r ? new Database\Result($r) : null;
	}

	/**
	 * @return \SwampyPHP\Database\Statement|null
	 */
	public function prepare(/*query [, $arg1...$argN]*/)
	{
		$statement = call_user_func_array(array($this, 'escapeQuery'), func_get_args());
	 	$this->_db->setAttribute(PDO::ATTR_STATEMENT_CLASS, array ('\SwampyPHP\Database\Statement', array($this)));

		/** @var \SwampyPHP\Database\Statement $stm */
		$stm = $this->_db->prepare($statement);
		return $stm;
	}

	/**
	 * Lock specific database tables
	 * @param string|string[] $tables
	 * @param string $lock_type
	 * @return int
	 */
	public function lock($tables, $lock_type = "WRITE")
	{
		return $this->exec('LOCK TABLES `'.(is_array($tables) ? implode('` '.$lock_type.', `', $tables) : $tables).'` '.$lock_type);
	}

	/**
	 * Unlock all tables
	 * @return int
	 */
	public function unlock()
	{
		return $this->exec("UNLOCK TABLES");
	}

	/**
	 * @param string $query, ...
	 * @return null|string
	 */
	public function getValue($query)
	{
		/** @var Result $result */
		$result = call_user_func_array(array($this, 'query'), func_get_args());
		return $result ? $result->value() : null;
	}

	/**
	 * @param string $query, ...
	 * @return null|string[]
	 */
	public function getRow($query)
	{
		/** @var Result $result */
		$result = call_user_func_array(array($this, 'query'), func_get_args());
		return $result ? $result->row() : null;
	}

	/**
	 * @param string $query, ...
	 * @return null|string[]
	 */
	public function getArray($query)
	{
		/** @var Result $result */
		$result = call_user_func_array(array($this, 'query'), func_get_args());
		return $result ? $result->assocArray() : null;
	}

	/**
	 * @param string $query, ...
	 * @return null|string[][]
	 */
	public function getTable($query /*, [$arg1...$argN, [$use_identifier = true]]*/)
	{
		/** @var Result $result */
		$result  = call_user_func_array(array($this, 'query'), func_get_args());
		return $result ? $result->table() : null;
	}

	/**
	 * @param string $query, ...
	 * @return int Number of affected rows
	 */
	public function affectRows($query)
	{
		return call_user_func_array(array($this, 'exec'), func_get_args());
	}

	/**
	 * @param string $query, ...
	 * @return int Inserted autoincrement value
	 */
	public function insertRow($query)
	{
		return call_user_func_array(array($this, 'exec'), func_get_args()) ? $this->lastInsertId() : false;
	}

	/**
	 * @return int
	 */
	public function lastInsertId()
	{
		return intval($this->_db->lastInsertId());
	}

	/**
	 * @param string $table Table name
	 * @param string[] $data Tble data
	 * @param string $condition
	 * @return int
	 */
	public function updateFromArray($table, $data, $condition)
	{
		$data = $this->_prepare($data, 1);

		$fields = array();
		foreach ($data as $field => $value)
			array_push($fields, $field."=".$value);

		return $this->affectRows("UPDATE `" . $table . "` SET " . implode(',', $fields) . " WHERE " . $condition);
	}

	/**
	 * @param string $table
	 * @param string[] $data
	 * @return int
	 */
	public function insertRowFromArray($table, $data)
	{
		$data = $this->_prepare($data, 1);

		/** @noinspection SqlResolve */
		return $this->insertRow("INSERT INTO `" . $table . "` (" . implode(',', array_keys($data)) . ") VALUES (" . implode(',', $data) . ")");
	}

	/**
	 * @param string $table
	 * @param string[] $data
	 * @return int
	 */
	public function insertOrUpdateRowFromArray($table, $data)
	{
		$data = $this->_prepare($data, 1);

		$fields = array();
		foreach ($data as $field => $value)
			array_push($fields, sprintf("%s=VALUES(%s)", $field, $field));

		/** @noinspection SqlResolve */
		return $this->affectRows
		(
			"INSERT INTO `" . $table . "` (" . join(',', array_keys($data)) . ") 
			VALUES (" . join(',', $data) . ") 
			ON DUPLICATE KEY UPDATE " . join(',', $fields)
		);
	}

	/**
	 * @param string $table
	 * @param string[][] $data
	 * @return int
	 */
	public function insertTableFromArray($table, $data)
	{
		$data = $this->_prepare($data, 2);

		$field_list = join(',', array_keys(current($data)));
		$values = array();

		foreach ($data as $row)
			array_push($values, sprintf(" (%s)", join(',', $row)));

		$value_list = join(",", $values);

		/** @noinspection SqlResolve */
		return $this->insertRow("INSERT INTO `" . $table . "` (". $field_list .") VALUES ". $value_list);
	}

	/**
	 * Enable/Disable debug
	 * @param bool $debug
	 */
	public function setDebug($debug = false)
	{
		$this->_debug = $debug;
	}

	/**
	 * Total time in seconds
	 * @return float
	 */
	public function getTotalTime()
	{
		return $this->_totalTime;
	}
}
