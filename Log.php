<?php
/**
 *  Logging helper class
 *
 *  SwampyPHP Framework
 *
 *  2008 - 2016 Solutera
 *
 *  @author     Domas Labokas <domas@solutera.lt>
 *  @copyright  Copyright (c) 2016, Solutera
 *  @version    1.0
 *  @license    End User License Agreement (EULA)
 *  @link       http://www.solutera.lt
 *
 */

namespace SwampyPHP;

class LogLevel
{
	const NONE      = 0;
	const FATAL     = 1;
	const ERROR     = 2;
	const WARNING   = 3;
	const INFO      = 4;
	const FIXME     = 5;
	const DEBUG     = 6;
	const TRACE     = 7;

	static private $_names = array
	(
		LogLevel::NONE      => 'none',
		LogLevel::FATAL  	=> 'fatal',
		LogLevel::ERROR     => 'error',
		LogLevel::WARNING   => 'warning',
		LogLevel::INFO      => 'info',
		LogLevel::FIXME     => 'fixme',
		LogLevel::DEBUG     => 'debug',
		LogLevel::TRACE     => 'trace',
	);

	static public function Name($level)
	{
		return isset(self::$_names[$level]) ? self::$_names[$level] : null;
	}
}

class Log
{
	/** @var Log */
	static private $_instance = null;

	/** @var string  */
	protected $_directory = null;

	/** @var string */
	protected $_dateFormat = 'Y-m-d G:i:s.u';

	/** @var string */
	protected $_filenameDateFormat = 'Y-m-d';

	/** @var string */
	protected $_filenameFormat = 'log_{date}.log';

	/** @var int */
	private $_flushFrequency = 10;

	/** @var int */
	private $_bufferedLineCount = 0;

	/** @var int */
	private $_backtraceLimit = 0;

	/** @var int */
	private $_logToFileLevel = LogLevel::NONE;

	/** @var int */
	protected $_level = LogLevel::NONE;

	/** @var string */
	private $_fileBuffer = "";

	/** @var string */
	private $_memBuffer = "";

	/**
	 * @return static
	 */
	public static function Instance()
	{
		if (self::$_instance == null)
			self::$_instance = new static();

		return self::$_instance;
	}

	/**
	 * @param string $message
	 * @param mixed $args, ... [optional]
	 */
	public static function Fatal($message, $args = null)
	{
		self::Instance()->_logArgs(LogLevel::FIXME, func_get_args());
		die();
	}

	/**
	 * @param string $message
	 * @param mixed $args, ... [optional]
	 */
	public static function Error($message, $args = null)
	{
		self::Instance()->_logArgs(LogLevel::ERROR, func_get_args());
	}

	/**
	 * @param string $message
	 * @param mixed $args, ... [optional]
	 */
	public static function Warning($message, $args = null)
	{
		self::Instance()->_logArgs(LogLevel::WARNING, func_get_args());
	}

	/**
	 * @param string $message
	 * @param mixed $args, ... [optional]
	 */
	public static function Info($message, $args = null)
	{
		self::Instance()->_logArgs(LogLevel::INFO, func_get_args());
	}

	/**
	 * @param string $message
	 * @param mixed $args, ... [optional]
	 */
	public static function Fixme($message, $args = null)
	{
		self::Instance()->_logArgs(LogLevel::FIXME, func_get_args());
	}

	/**
	 * @param string $message
	 * @param mixed $args, ... [optional]
	 */
	public static function Debug($message, $args = null)
	{
		self::Instance()->_logArgs(LogLevel::DEBUG, func_get_args());
	}

	/**
	 * @param string $message
	 * @param mixed $args, ... [optional]
	 */
	public static function Trace($message, $args = null)
	{
		self::Instance()->_logArgs(LogLevel::TRACE, func_get_args());
	}

	/**
	 * Simple log method
	 *
	 * @param string $level
	 * @param string $message
	 * @param mixed $args, ... [optional]
	 */
	public function log($level, $message, $args = null)
	{
		$args = func_get_args();
		array_shift($args);
		$this->_logArgs($level, $args);
	}

	/**
	 * Full log method
	 *
	 * @param string $file
	 * @param int $line
	 * @param string $class
	 * @param string $func
	 * @param string $level
	 * @param string $message
	 * @param mixed $args, ... [optional]
	 */
	public function fullLog($file, $line, $class, $func, $level, $message, $args = null)
	{
		if ($this->_level < $level)
			return;

		$args = func_get_args();
		$args = array_slice($args, 5);
		$args = $this->_filterArgs($args);

		$message = call_user_func_array('sprintf', $args);

		$this->fixedLog($file, $line, $class, $func, $level, $message);
	}

	protected function fixedLog($file, $line, $class, $func, $level, $message)
	{
		$logLine = date($this->_dateFormat) . ' ';

		$path = explode('/', $file);
		$logLine .= '[' . strtoupper(LogLevel::Name($level)) . '] ';
		$logLine .= '{' . $_SERVER['REMOTE_ADDR'] .'}';
		if ($file) $logLine .=  ' ' . end($path);
		if ($line) $logLine .= '(' . $line . ')';
		if ($class) $logLine .= ' - ' . $class . '::';
		if ($func) $logLine .= $func;

		$logLine .= ' ' . $message;

		$this->_bufferPush($logLine, $level);
	}

	/**
	 * Destructor
	 */
	public function __destruct()
	{
		$this->_flush();
	}

	/**
	 * @param int $level
	 */
	public function setLevel($level)
	{
		$this->_level = $level;
		if ($level >= LogLevel::TRACE)
			$this->setBacktraceLimit($level - LogLevel::TRACE + 1);
	}

	/**
	 * @param int $level
	 */
	public function setLogToFileLevel($level)
	{
		$this->_logToFileLevel = $level;
	}

	/**
	 * @param string $directory
	 */
	public function setDirectory($directory)
	{
		$this->_directory = $directory;
	}

	/**
	 * @param string $filenameFormat
	 */
	public function setFilenameFormat($filenameFormat)
	{
		$this->_filenameFormat = $filenameFormat;
	}

	/**
	 * @param string $filenameDateFormat
	 */
	public function setFilenameDateFormat($filenameDateFormat)
	{
		$this->_filenameDateFormat = $filenameDateFormat;
	}

	/**
	 * @param int $backtraceLimit
	 */
	public function setBacktraceLimit($backtraceLimit)
	{
		$this->_backtraceLimit = $backtraceLimit;
	}

	/**
	 * @return string
	 */
	public function getMemBuffer()
	{
		return $this->_memBuffer;
	}

	private function _log($level, $message, $args = null)
	{
		if ($this->_level < $level)
			return;

		$args = func_get_args();

		$file = null;
		$class = null;
		$func = null;
		$type = null;
		$line = 0;

		if ($this->_backtraceLimit)
		{
			$limit = 4 + $this->_backtraceLimit;

			$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $limit);

			if (count($backtrace) > 5)
				$this->_bufferPush("", $level);

			for ($i = count($backtrace) - 1; $i >= 4; $i--)
			{
				if ($file)
				{
					$file = explode('/', $file);
					$this->_bufferPush("\t" . end($file) . '(' . $line . ') ' . $class . $type . $func, $level);
				}

				$t = $backtrace[$i];

				$file = isset($backtrace[$i-1]['file']) ? $backtrace[$i-1]['file'] : null;
				$line = isset($backtrace[$i-1]['line']) ? $backtrace[$i-1]['line'] : 0;
				$class = isset($t['class']) ? $t['class'] : null;
				$func =  isset($t['function']) ? $t['function'] : null;
				$type =  isset($t['type']) ? $t['type'] : ' ';
			}
		}

		$args = array_merge(array($file, $line, $class, $func), $args);

		call_user_func_array(array($this, 'fullLog'), $args);
	}

	/**
	 * @param $level
	 * @param array $args
	 */
	private function _logArgs($level, array $args)
	{
		if ($this->_level < $level)
			return;

		array_unshift($args, $level);
		call_user_func_array(array($this, '_log'), $args);
	}

	/**
	 * Conver all argument to string
	 *
	 * @param array $args
	 * @return array
	 */
	private static function _filterArgs(array $args)
	{
		foreach($args as $i => $element)
		{
			if (is_array($element) || is_object($element))
				$args[$i] = json_encode($element);
			elseif (is_null($element))
				$args[$i] = '{null}';
		}

		return $args;
	}

	private function _bufferPush($string, $level)
	{
		$this->_memBuffer .= $string . "\n";

		if ($this->_logToFileLevel < $level)
			return;

		$this->_fileBuffer .= $string . "\n";

		$this->_bufferedLineCount++;
		if ($this->_bufferedLineCount >= $this->_flushFrequency)
			$this->_flush();
	}

	/**
	 * Flush buffer to file
	 */
	private function _flush()
	{
		if ($this->_logToFileLevel && $this->_directory)
		{
			if (!file_exists($this->_directory))
			{
				if (!mkdir($this->_directory, 0777, true))
					throw new \Exception('Unable to create director at "' . $this->_directory . '"');
			}

			$date = date($this->_filenameDateFormat);
			$filename = str_replace('{date}', $date, $this->_filenameFormat);

			file_put_contents($this->_directory . $filename, $this->_fileBuffer, FILE_APPEND);
		}

		$this->_fileBuffer = "";
		$this->_bufferedLineCount = 0;
	}
}
