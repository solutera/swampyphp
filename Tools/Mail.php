<?php

/**
 *  Mail Class - Simple mail class that support multiple body types and attachments
 *
 *  Swampy-PHP Framework
 *
 *  2008 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.swampyfoot.com
 *   
 */

namespace SwampyPHP\Tools;

use SwampyPHP\Object;

class Mail extends Object
{
	const BODY_TYPE_TEXT = 'text/plain';
	const BODY_TYPE_HTML = 'text/html';

	const ENCODING_UTF8 = 'utf-8';

	const EOL = "\r\n";

	/** @var array  */
	private $_to = array();
	/** @var array  */
	private $_cc = array();
	/** @var array  */
	private $_bcc = array();
	/** @var array  */
	private $_subject = array();
	/** @var string  */
	private $_from = null;
	/** @var string  */
	private $_reply_to = null;
	/** @var array  */
	private $_body = array();
	/** @var array  */
	private $_attachments = array();

	/**
	 * @param string $email
	 */
	public function setTo($email)
	{
		array_push($this->_to, $email);
	}

	/**
	 * @param string $email
	 */
	public function setCC($email)
	{
		array_push($this->_cc, $email);
	}

	/**
	 * @param string $email
	 */
	public function setBCC($email)
	{
		array_push($this->_bcc, $email);
	}

	/**
	 * @param string $subject
	 */
	public function setSubject($subject)
	{
		$this->_subject = $subject;
	}

	/**
	 * @param string $email
	 * @param string|null $name
	 */
	public function setFrom($email, $name = null)
	{
		$this->_from = $name ? ($name . ' <' . $email . '>') : $email;    //"$name <$address>" . self::EOL;

		if (!$this->_reply_to)
			$this->_reply_to = $email;
	}

	/**
	 * @param string $email
	 */
	public function setReplyTo($email)
	{
		$this->_reply_to = $email;			
	}

	/**
	 * @param string $body
	 * @param string $type
	 * @param string $encoding
	 */
	public function setBody($body, $type = self::BODY_TYPE_TEXT, $encoding = self::ENCODING_UTF8)
	{
		$text = ($encoding == self::ENCODING_UTF8) ? $body : mb_convert_encoding($body, self::ENCODING_UTF8, $encoding);	
		array_push($this->_body, array('text'=>$text, 'type'=>$type));
	}

	/**
	 * @param string $file
	 * @param string|null $name
	 * @param string|null $type
	 */
	public function attachFile($file, $name = null, $type = null)
	{
		array_push($this->_attachments, array('file'=>$file, 'name'=>$name, 'type'=>$type));
	}

	/**
	 * @return bool
	 */
	public function send()
	{
		$headers = array();
	
		$headers['MIME-Version'] = 	'1.0';
		//$headers['To'] = 		implode(', ', $this->_to);
		//$headers['Reply-To'] = 	$this->_reply_to;
		$headers['From'] = 		$this->_from;
		$headers['Return-Path'] = 	$this->_from;
		$headers['X-Mailer'] = 		'PHP/' . phpversion();
		$headers['Message-ID'] = '<'.time().' system@'.$_SERVER['SERVER_NAME'].'>';

		if (count($this->_cc))
			$headers['Cc'] = implode(', ', $this->_cc);

		if (count($this->_bcc))
			$headers['Bcc'] = implode(', ', $this->_bcc);

		$body = '';	
		$hash =  md5(date('r', time()));
		
		$alternative = (count($this->_body) > 1);

		// Set email content type
		if (count($this->_attachments))
			$headers['Content-Type'] = 'multipart/mixed; boundary="PHP-mixed-'.$hash.'"';
		elseif ($alternative)
			$headers['Content-Type'] = 'multipart/alternative; boundary="PHP-alt-'.$hash.'"';
		elseif (count($this->_body))
		{
			$headers['Content-Type'] = $this->_body[0]['type'];
			$headers['Content-Transfer-Encoding'] = 'quoted-printable';
		}
		else
			return false;


		if (count($this->_attachments))
			$body .= '--PHP-mixed-'.$hash . self::EOL;

		if ($alternative && count($this->_attachments))
			$body .= 'Content-Type: multipart/alternative; boundary="PHP-alt-'.$hash.'"' . self::EOL . self::EOL;;
		
		
		// Set body content
		foreach ($this->_body as $b)
		{
			if ($alternative)
				$body .= '--PHP-alt-' . $hash . self::EOL;

			if ($alternative || count($this->_attachments))
			{
				$body .= 'Content-Type: '.$b['type'].'; charset=UTF-8' . self::EOL;
				$body .= 'Content-Transfer-Encoding: quoted-printable' . self::EOL . self::EOL;
			}
			
			$body .= quoted_printable_encode($b['text']) . self::EOL;			
			$body .= self::EOL;
		}

		if ($alternative)
			$body .= '--PHP-alt-' . $hash . '--' . self::EOL . self::EOL;

		// Add attachment
		foreach ($this->_attachments as $a)
		{
			if (file_exists($a['file']))
			{
				$file = $a['file'];
			
				// Get file name
				$name = $a['name'] ? $a['name'] : basename($file);

				// Get file type
				$type = $a['type'];
				if(!$type)
				{
					$finfo = finfo_open(FILEINFO_MIME_TYPE);
					$type = finfo_file($finfo, $file);
					finfo_close($finfo);
				}
				
				$body .= '--PHP-mixed-' . $hash . self::EOL;
				$body .= 'Content-Type: '.$type.'; name="'.$name.'"' . self::EOL;
				$body .= 'Content-Transfer-Encoding: base64' . self::EOL;
				$body .= 'Content-Disposition: attachment'. self::EOL;
				$body .= self::EOL;
				$body .= chunk_split(base64_encode(file_get_contents($file))) . self::EOL;
			}
		}

		if (count($this->_attachments))
			$body .= '--PHP-mixed-'.$hash.'--' . self::EOL;

		$head = '';

		foreach ($headers as $name => $value)
			$head .= $name.': '.$value . self::EOL;
		
		return mail(implode(', ', $this->_to), '=?UTF-8?B?'.base64_encode($this->_subject).'?=', $body, $head, '-f'.$this->_reply_to);
	}
}
