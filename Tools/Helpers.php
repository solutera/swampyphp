<?php

/**
 *  Helpers Class
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\Tools;

if (!function_exists('getallheaders'))
{
	/**
	 * @return array
	 */
	function getallheaders()
	{
		$headers = array();

		foreach ($_SERVER as $name => $value)
		{
			if (substr($name, 0, 5) == 'HTTP_')
			{
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}

		return $headers;
	}
}

class Helpers
{
	/**
	 * @param string $name
	 * @param mixed|null $default
	 * @return mixed|null
	 */
	public static function getValue($name, $default = null)
	{
		return self::getArrayValue($_GET, $name, $default); //isset($_GET[$name]) ? $_GET[$name] : $default;
	}

	/**
	 * @param string $name
	 * @param mixed|null $default
	 * @return mixed|null
	 */
	public static function getPostValue($name, $default = null)
	{
		if (isset($_SERVER["CONTENT_TYPE"]) && $_SERVER["CONTENT_TYPE"] == 'application/json')
		{
			$json = json_decode(file_get_contents('php://input'), true);
			$_POST = is_array($json) ? $json : [];
		}

		return self::getArrayValue($_POST, $name, $default); //isset($_POST[$name]) ? $_POST[$name] : $default;
	}

	/**
	 * @param array $array
	 * @param string $name
	 * @param mixed|null $default
	 * @return mixed|null
	 */
	private static function getArrayValue($array, $name, $default = null)
	{
		$tree = explode('[', str_replace(']', '', $name));
		$value = $array;

		foreach ($tree as $var)
		{
			$value = (is_array($value) && isset($value[$var])) ? $value[$var] : null;

			if ($value === null)
				return $default;
		}

		return $value;
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public static function isSubmit($name)
	{
		return (isset($_POST[$name]) || isset($_GET[$name]));
	}

	/**
	 * @param mixed $value
	 * @param mixed $default
	 * @return mixed
	 */
	public static function ifNull($value, $default)
	{
		return is_null($value) ? $default : $value;
	}

	/**
	 * @param mixed $value
	 * @param mixed $default
	 * @return mixed
	 */
	public static function ifEmpty($value, $default)
	{
		return empty($value) ? $default : $value;
	}

	/**
	 * @param string $location
	 * @param bool $permanent
	 */
	public static function Redirect($location, $permanent = false)
	{
		if ($permanent)
			header("HTTP/1.1 301 Moved Permanently");

		header('Location: '.$location);
		exit;
	}

	/**
	 * @param array|string $array
	 * @return array|string
	 */
	public static function StripSlashes($array)
	{
		$marray = array();

		if (is_array($array))
		{
			foreach ($array as $key => $value)
				$marray[$key] = Helpers::StripSlashes($value);

			return $marray;
		}
		else
			return stripslashes($array);
	}

	/**
	 * @param int|string $key
	 * @return null
	 */
	public static function GetUploadedFile($key)
	{
		if (isset($_FILES[$key]) && is_uploaded_file($_FILES[$key]['tmp_name']))
			return $_FILES[$key];

		return null;
	}

	/**
	 * @return array|null
	 */
	public static function GetStreamFile()
	{
		$size = isset($_SERVER["CONTENT_LENGTH"]) ? $_SERVER["CONTENT_LENGTH"] : 0;

		$tmp = new TempFile('php_input_');

		// Open stream & temp file
		$src_fd = fopen("php://input", "r");
		$tmp->open('w');

		// Copy data
		$src_size = stream_copy_to_stream($src_fd, $tmp->fd);

		// Close files
		fclose($src_fd);
		$tmp->close();

		// Validate size
		if($src_size != $size)
			return null;

		// Extract files info
		$h = getallheaders();
		$finfo = finfo_open(FILEINFO_MIME);

		// Set uploaded file info
		$file = array();
		$file['size'] = $src_size;
		$file['name'] = isset($h['X-File-Name']) ? $h['X-File-Name'] : null;
		$file['tmp_name'] = $tmp->name;
		$file['type'] = finfo_file($finfo, $tmp->name);
		$file['object'] = $tmp;

		return $file;
	}
}
