<?php
/**
 *  TempFile Class - For auto deleting file use
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\Tools;

class TempFile
{
	/** @var resource  */
	public $fd = null;
	/** @var string  */
	public $name = null;

	/**
	 * TempFile constructor.
	 * @param string $prefix
	 */
	public function __construct($prefix = 'temp_')
	{
		$this->name = tempnam(sys_get_temp_dir(), $prefix);
	}

	public function __destruct()
	{
		$this->close();

		if ($this->name)
			unlink($this->name);
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->name;
	}

	/**
	 * @param string $mode
	 * @return resource
	 */
	public function open($mode)
	{
		return ($this->fd = fopen($this->name, $mode));
	}

	public function close()
	{
		if($this->fd)
			fclose($this->fd);

		$this->fd = null;
	}
}
