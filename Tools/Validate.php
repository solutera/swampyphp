<?php
/**
 *  Validation helper class
 *
 *  Swampy-PHP Framework
 *
 *  2008 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @author    Vidas Silius <vidas@solutera.lt>
 *  @copyright Copyright (c) 2016, Solutera
 *  @version   1.1
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\Tools;

use DateTime;

class Validate
{
	/**
	 * @param string $string
	 * @return bool
	 */
	public static function isEmail($string)
	{
		return filter_var($string, FILTER_VALIDATE_EMAIL);
	}

	/**
	 * @param string $string
	 * @param mixed $filter_options
	 * @return bool
	 */
	public static function isURL($string, $filter_options = null)
	{
		return filter_var($string, FILTER_VALIDATE_URL, $filter_options);
	}

	/**
	 * @param string $string
	 * @return bool
	 */
	public static function isIP($string)
	{
		return filter_var($string, FILTER_VALIDATE_IP);
	}

	/**
	 * @param string $string
	 * @return bool
	 */
	public static function isUnsignedInt($string)
	{
		return preg_match('/^[0-9]+$/', $string);
	}

	/**
	 * @param string $string
	 * @param string $valid_symbols
	 * @return bool
	 */
	public static function isText($string, $valid_symbols = ' ')
	{
		return $string ? preg_match('/^[\p{L}0-9'.preg_quote($valid_symbols, '/').']+$/iu', $string) : false;
	}

	/**
	 * @param string $string
	 * @return bool
	 */
	public static function isName($string)
	{
		return $string ? preg_match('/^[\p{L} \-]+$/iu', $string) : false;
	}

	/**
	 * @param string $string
	 * @return bool
	 */
	public static function isTimestamp($string)
	{
		return $string ? preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/', $string) : false;
	}

	/**
	 * @param mixed $var
	 * @return bool
	 */
	public static function isNotNull($var)
	{
		return !is_null($var);
	}

	/**
	 * @param mixed $var
	 * @return bool
	 */
	public static function isNull($var)
	{
		return is_null($var);
	}

	/**
	 * @param object $object
	 * @return bool
	 */
	public static function isObject($object)
	{
		return is_object($object);
	}

	/**
	 * @param mixed $src
	 * @return bool
	 */
	public static function isImage($src)
	{
		return (($info = getimagesize($src)) && $info[0] && $info[1]);		
	}

	/**
	 * @param mixed $src
	 * @return bool
	 */
	public static function isValidImageSize($src)
	{
		list($width, $height) = getimagesize($src);
		$memory_limit = ini_get('memory_limit');
		$memory_limit_bytes = substr($memory_limit, 0, strlen($memory_limit)-1) * 1024 * 1024;

		return $width * $height * 4 * 2 * 1.2 <= $memory_limit_bytes;
	}

	/**
	 * @param string $date
	 * @param string $format
	 * @return bool
	 */
	public static function isValidDateTime($date, $format = 'Y-m-d H:i:s')
	{
		$d = DateTime::createFromFormat($format, $date);

		return $d && $d->format($format) == $date;
	}
}
