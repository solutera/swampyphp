<?php

/**
 *  Image Class - Class for scaling and cropping image
 *
 *  Swampy-PHP Framework
 *
 *  2008 - 2015 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, UAB Solutera
 *  @version   1.1
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\Tools;

class Image
{
	const ERROR_NONE = 0;
	const ERROR_SRC_INVALID = 1;
	const ERROR_SRC_UNSUPPORTED = 2;
	const ERROR_SRC_CORRUPTED = 3;
	const ERROR_MASK_INVALID = 4;
	const ERROR_OUTPUT = 5;
	
	const SCALE_NONE = 'none';
	const SCALE_ADD_BG = 'addbg';
	const SCALE_CROP = 'crop';
	const SCALE_TOP_CROP = 'topcrop';
	const SCALE_LEFT_CROP = 'leftcrop';
	const SCALE_MAX_FIT = 'max';
	const SCALE_STRETCH = 'stretch';

	/** @var int  */
	private static $last_error = 0;

	/**
	 * @return int
	 */
	public static function LastError()
	{
		return self::$last_error;
	}

	/**
	 * @param int $error
	 * @return bool
	 */
	private static function Error($error = self::ERROR_NONE)
	{
		self::$last_error = $error;
		return ($error == self::ERROR_NONE) ? true : false;
	}

	/**
	 * @param string $src_path
	 * @param string|null $dst_path
	 * @param int $width
	 * @param int $height
	 * @param string|null $ext
	 * @return bool
	 */
	public static function Crop($src_path, $dst_path = null, $width = 0, $height = 0, $ext = null)
	{
		return self::Resample($src_path, $dst_path, $width, $height, $ext, self::SCALE_CROP);
	}

	/**
	 * @param string $src_path
	 * @param string|null $dst_path
	 * @param int $width
	 * @param int $height
	 * @param string|null $ext
	 * @return bool
	 */
	public static function Resize($src_path, $dst_path = null, $width = 0, $height = 0, $ext = null)
	{
		return self::Resample($src_path, $dst_path, $width, $height, $ext, self::SCALE_ADD_BG, 'FFFFFF00');
	}

	/**
	 * @param string $src_path
	 * @param string $mask_path
	 * @param bool $output
	 * @return bool
	 */
	public static function AddMask($src_path, $mask_path, $output = false)
	{
		return self::Resample($src_path, $output ? null : $src_path, 0, 0, null, self::SCALE_NONE, 'FFFFFF00', $mask_path);
	}

	/**
	 * @param string $src_path
	 * @param string|null $dst_path
	 * @param int $width
	 * @param int $height
	 * @param string|null $ext
	 * @param string $scale
	 * @param string|null $bg_color
	 * @param string|null $mask_path
	 * @return bool
	 */
	public static function Resample($src_path, $dst_path = null, $width = 0, $height = 0, $ext = null, $scale = self::SCALE_NONE, $bg_color = null, $mask_path = null)
	{
		$srcInfo = getimagesize($src_path);// Getting source image info width height type

		if(!$srcInfo)
			return self::Error(self::ERROR_SRC_INVALID);

		list($src_w, $src_h, $src_type) = $srcInfo;

		// Load image by its type
		switch($src_type)
		{
			case 1:// GIF
				$src_ext = "gif";
				$srcImage = @ imagecreatefromgif($src_path);
				break;
			case 2:// JPEG
				$src_ext = "jpg";
				$srcImage = @ imagecreatefromjpeg($src_path);
				break;
			case 3:// PNG
				$src_ext = "png";
				$srcImage = @ imagecreatefrompng($src_path);
				break;
			default:
				return self::Error(self::ERROR_SRC_UNSUPPORTED);
		}

		// Check if image is loaded
		if (!$srcImage)
			return self::Error(self::ERROR_SRC_CORRUPTED);

		// Source image size ratio
		$src_ratio = $src_w / $src_h;

		// Set destination image size
		if (!$width && $height)
			$width = round($height * $src_ratio);

		if ($width && !$height)
			$height = round($width / $src_ratio);

		if (!$width && !$height)
		{
			$width = $src_w;
			$height = $src_h;
		}

		// Destination image ratio
		$dst_ratio = $width / $height;

		// Initial position coordinates
		$dst_x = $dst_y = $src_x = $src_y = 0;

		switch ($scale)
		{
			case self::SCALE_ADD_BG:
				$dst_w = ($dst_ratio > $src_ratio) ? round($height * $src_ratio) : $width;
				$dst_h = ($dst_ratio < $src_ratio) ? round($width / $src_ratio) : $height;

				if ($width > $src_w && $height > $src_h)
				{
					$dst_w = $src_w;
					$dst_h = $src_h;
				}

				$dst_x = ($width/2) - ($dst_w/2);
				$dst_y = ($height/2) - ($dst_h/2);

				break;
			case self::SCALE_CROP:
			case self::SCALE_TOP_CROP:
			case self::SCALE_LEFT_CROP:
				$w = ($width > $src_w) ? $src_w : $width;
				$h = ($height > $src_h) ? $src_h : $height;
				$dst_w = ($dst_ratio < $src_ratio) ? round($h * $src_ratio) : $w;
				$dst_h = ($dst_ratio > $src_ratio) ? round($w / $src_ratio) : $h;
				$dst_x = ($width/2) - ($dst_w/2);
				$dst_y = ($height/2) - ($dst_h/2);
				
				if ($scale == self::SCALE_TOP_CROP)
					$dst_y = 0;

				if ($scale == self::SCALE_LEFT_CROP)
					$dst_x = 0;
				
				break;
			case self::SCALE_MAX_FIT:
				if ($width > $src_w && $height > $src_h)
				{
					$width = $src_w;
					$height = $src_h;
					$dst_ratio = $width / $height;
				}

				$width = $dst_w = ($dst_ratio > $src_ratio) ? round($height * $src_ratio) : $width;
				$height = $dst_h = ($dst_ratio < $src_ratio) ? round($width / $src_ratio) : $height;

				break;
			case self::SCALE_STRETCH:
				$dst_w = $width;
				$dst_h = $height;

				break;
			case self::SCALE_NONE:
			default:
				$width = $dst_w = $src_w;
				$height = $dst_h = $src_h;

				break;
		}

		$ext = $ext ? strtolower($ext) : $src_ext;
		$dstImage = imagecreatetruecolor($width, $height);

		if (!$mask_path && $ext != 'jpg')
		{
			imagecolortransparent($dstImage, imagecolorallocate($dstImage, 255, 255, 255));
			imagealphablending($dstImage, false);
			imagesavealpha($dstImage, true);
		}

		// Creates image background color
		if (in_array($scale, array(self::SCALE_ADD_BG, self::SCALE_CROP, self::SCALE_TOP_CROP, self::SCALE_LEFT_CROP, self::SCALE_MAX_FIT)))
		{
			if (!$bg_color)
				$bg_color = ($ext == 'jpg') ? 'FFFFFF' : 'FFFFFF00';

		
			$r = hexdec(substr($bg_color,0,2));
			$g = hexdec(substr($bg_color,2,2));
			$b = hexdec(substr($bg_color,4,2));

			if (strlen($bg_color) == 8)
			{
				$a = 127 - hexdec(substr($bg_color,6,2)) / 2;
				$color = imagecolorallocatealpha($dstImage, $r, $g, $b, $a);
			}
			else
			{
				$color = imagecolorallocate($dstImage, $r, $g, $b);
			}

			imagefill($dstImage, 0, 0, $color);
		}

		imagecopyresampled($dstImage, $srcImage, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
		imagedestroy($srcImage);

		// Adding image mask
		if ($mask_path)
		{
			$maskImage = imagecreatefrompng($mask_path);

			if (!$maskImage)
				return self::Error(self::ERROR_MASK_INVALID);

			imagealphablending($maskImage, 1);
			imagecopy($dstImage, $maskImage, 0, 0, 0, 0, $dst_w, $dst_h);
			imagedestroy($maskImage);
		}

		$return = false;
		switch ($ext)	//creating image by selected extension format
		{
			case "jpeg":
			case "jpg":
				$return = imagejpeg($dstImage, $dst_path, 90);
				break;
			case "png":
				$return = imagepng($dstImage, $dst_path, 9);
				break;
			case "gif":
				$return = imagegif($dstImage, $dst_path);
				break;
			case "bmp":
				$return = imagewbmp($dstImage, $dst_path);
				break;
		}

		imagedestroy($dstImage);

		return $return ? self::Error(self::ERROR_NONE) : self::Error(self::ERROR_OUTPUT);
	}
}
