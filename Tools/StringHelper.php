<?php
/**
 *  String Helper class
 *
 *  Swampy-PHP Framework
 *
 *  2008 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP\Tools;

class StringHelper
{
	/**
	 * Remove any non latin (A-Z) and non numeric (0-9) characters
	 *
	 * @param string $string input
	 * @param string $space character used as word separator
	 * @return string
	 */
	public static function Sanitize($string, $space = ' ')
	{
		$filters = array
		(
			'/[^A-Za-z0-9\- ]/' => ' ',
			'/[\-]+/' => $space,
			'/\s+/' => $space
		);

		return trim(preg_replace(array_keys($filters), $filters, $string));
	}

	/**
	 * Transliterate (convert foreign symbols to latin)
	 *
	 * @param string $string
	 * @param string|null $locale
	 * @return string
	 */
	public static function Transliterate($string, $locale = null)
	{
		if ($locale)
			setlocale(LC_ALL, $locale);

		return transliterator_transliterate("Any-Latin; Latin-ASCII;", $string);
	}

	/**
	 * Tokenize - Convert string into token array
	 *
	 * @param string $string
	 * @return string[]
	 */
	public static function ToKeywords($string)
	{
		$string = strip_tags($string);
		$string = self::Transliterate($string);
		$string = self::Sanitize($string, ' ');
		$string = strtolower($string);
		$string = trim($string);

		$words = explode(' ', $string);

		foreach ($words as $i => $word)
			if (strlen($word) > 64)
				$words[$i] = substr($word, 0, 64);

		return $words;
	}
}
