<?php

/**
 *  REST API Class
 *
 *  Swampy-PHP Framework
 *
 *  2014 - 2016 Solutera
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2016, UAB Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *
 */

namespace SwampyPHP;

use SwampyPHP\Database\Model;
use SwampyPHP\HTTP\Connection;
use SwampyPHP\HTTP\ContentType;
use SwampyPHP\HTTP\Exception;
use SwampyPHP\HTTP\Header\Authorization;
use SwampyPHP\HTTP\HeaderField;
use SwampyPHP\HTTP\Method;
use SwampyPHP\HTTP\Request;
use SwampyPHP\HTTP\Response;
use SwampyPHP\HTTP\StatusCode;
use SwampyPHP\Model\Error;

class Credentials extends Object
{
	/** @var string  */
	public $username = null;
	/** @var string  */
	public $password = null;

	/**
	 * @param string $username
	 * @param string $password
	 * @return Credentials
	 */
	public static function Credentials($username, $password)
	{
		$credentials = Credentials::Object();
		$credentials->username = $username;
		$credentials->password = $password;

		return $credentials;
	}
}

class Route extends Object
{
	/** @var string */
	public $uri_pattern = null;
	/** @var string */
	public $method = null;
	/** @var callable */
	public $callback = null;
	/** @var Object|string */
	public $model = null;
	/** @var array */
	public $matches = null;
}

abstract class RestAPI extends Connection
{
	/** @var Request  */
	protected $request = null;
	/** @var Response  */
	protected $response = null;
	/** @var array|Route[]  */
	protected $routes = array();
	/** @var string  */
	protected $powered_by = 'RestAPI';
	/** @var string  */
	protected $version = '1.0';
	/** @var callable  */
	protected $authentication_challenge_callback = null;

	/**
	 * @param mixed $message
	 */
	protected function log($message)
	{
		static $count = 0;

		if ($this->response)
			$this->response->setHeader('X-Log-'.$count++, json_encode($message));
	}

	/**
	 * @param string $method
	 * @param string $uri_pattern
	 * @param Object|string $model
	 * @param callable $callback
	 */
	protected function handle($method, $uri_pattern, $model, $callback)
	{
		$route = Route::Object();
		$route->method = $method;
		$route->uri_pattern = $uri_pattern;
		$route->model = $model;
		$route->callback = $callback;

		array_push($this->routes, $route);
	}

	/**
	 * @param string $uri_pattern
	 * @param Object|string $model
	 * @param callable $callback
	 */
	protected function handleGet($uri_pattern, $model, $callback)
	{
		$this->handle(Method::GET, $uri_pattern, $model, $callback);
	}

	/**
	 * @param string $uri_pattern
	 * @param Object|string $model
	 * @param callable $callback
	 */
	protected function handlePost($uri_pattern, $model, $callback)
	{
		$this->handle(Method::POST, $uri_pattern, $model, $callback);
	}

	/**
	 * @param string $uri_pattern
	 * @param Object|string $model
	 * @param callable $callback
	 */
	protected function handlePut($uri_pattern, $model, $callback)
	{
		$this->handle(Method::PUT, $uri_pattern, $model, $callback);
	}

	/**
	 * @param string $uri_pattern
	 * @param callable $callback
	 */
	protected function handleDelete($uri_pattern, $callback)
	{
		$this->handle(Method::DELETE, $uri_pattern, null, $callback);
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @return boolean
	 */
	abstract protected function processRequestHandler(Request $request, Response $response);

	/**
	 * @param Authorization $auth
	 * @return Credentials
	 */
	protected function handleAuthenticationChallenge(Authorization $auth)
	{
		return null;
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @throws Exception
	 */
	protected function validateAuthentication($username, $password)
	{
		$auth = $this->request->getAuthorization();

		// Validate authentication
		if ($auth && $auth->opaque && $auth->type == Authorization::TYPE_DIGEST)
		{
			session_id($auth->opaque);
			session_start();

			$url = URL::Object();
			$auth->uri = $url->path;
			$auth->method = $this->request->getMethod();
			$auth->nonce = isset($_SESSION['auth-nonce']) ? $_SESSION['auth-nonce'] : md5(uniqid().time());

			unset($_SESSION['auth-nonce']);

			if ($auth->validateCredentials($username, $password))
				return;
		}

		if (!session_id())
			session_start();

		// Create auth request
		$auth = Authorization::Object();
		$auth->type = Authorization::TYPE_DIGEST;
		$auth->realm = $this->powered_by;
		$auth->opaque = session_id();
		$auth->nonce = $_SESSION['auth-nonce'] = md5(uniqid().time());

		// Send response with auth requirement
		$this->response->setWWWAuthenticate($auth);

		throw new Exception(StatusCode::UNAUTHORISED);
	}

	/**
	 * @param Request $request
	 * @return Response
	 * @throws Exception
	 */
	protected function handleRequest(Request $request)
	{
		$start_time = microtime(true);

		$this->request = $request;
		$this->response = $response = Response::Alloc()->initWithIncomingRequest($this->request);

		try
		{
			$this->processRequestHandler($request, $response);

			/** @var Route $found */
			$found = null;
			$allowed_methods = array();

			/** @var Route $route */
			foreach ($this->routes as $route)
			{
				$pattern = '/^' . str_replace(array('%d', '%s'), array('([0-9]+)', '([0-9a-zA-Z\-_]+)'),  preg_quote($route->uri_pattern, '/')) . '$/';
				if (preg_match($pattern, $request->url->path, $route->matches))
				{
					array_push($allowed_methods, $route->method);

					if ($route->method == $this->request->method)
						$found = $route;
				}
			}

			if ($this->request->method == Method::OPTIONS && count($allowed_methods))
			{
				$this->response->setHeader(HeaderField::ALLOW, implode(",", $allowed_methods));
			}
			elseif ($found)
			{
				if (is_array($found->callback) && ($class_name = reset($found->callback)) && is_string($class_name))
				{
					/** @var RestAPI $class_name */
					$handler = $class_name::Object();

					if ($handler instanceof RestAPI)
					{
						$handler->request = $this->request;
						$handler->response = $this->response;
						$found->callback[0] = $handler;
					}
				}

				if (!is_callable($found->callback))
					throw new Exception(StatusCode::INTERNAL_SERVER_ERROR);

				// Get function params
				$params = $found->matches;
				array_shift($params);

				// Add model to params
				if ($found->model)
					array_push($params, $found->model);


				// Validate incoming data
				if ($found->model && in_array($found->method, array(Method::PUT, Method::POST)))
				{
					$data = $this->request->getData();

					if (!is_array($data) || !count($data))
						throw new Exception(StatusCode::BAD_REQUEST);

					$found->model->setVars($data);
				}

				// Exec request handler
				$response_data = call_user_func_array($found->callback, $params);

				// Check response data
				if (!$response_data)
					throw new Exception(StatusCode::INTERNAL_SERVER_ERROR);

				// Set response data
				if (in_array($found->method, array(Method::GET, Method::POST)))
				{
					// Set new route location header
					if ($found->method == Method::POST && is_object($response_data))
					{
						if (isset($response_data->id) && $response_data->id)
						{
							$url = $request->getURL();
							$url->path .= $found->model->id;
							$response->setHeader(HeaderField::LOCATION, $url);
						}

						$response->setStatusCode(StatusCode::CREATED);
					}
					else
						$response->setStatusCode(StatusCode::OK);

					// Set response data
					if ($found->model)
						$response->setData($response_data);
				}
			}
			elseif (count($allowed_methods))
				throw new Exception(StatusCode::METHOD_NOT_ALLOWED);
			else
				throw new Exception(StatusCode::NOT_IMPLEMENTED);
		}
		catch (\Exception $e)
		{
			$code = $e->getCode();

			$pathinfo = pathinfo($e->getFile());

			$error = Error::Error($code, $e->getMessage(), $e->getLine(), $pathinfo['filename']);

			if (!StatusCode::Description($code))
				$code = StatusCode::INTERNAL_SERVER_ERROR;

			$this->response->setContentType(ContentType::APPLICATION_JSON);
			$this->response->setStatusCode($code);
			$this->response->setData($error);
		}

		$response->setHeader('X-Powered-By', $this->powered_by);
		$response->setHeader('X-Version', $this->version);
		$response->setHeader('X-Load-Time', number_format((microtime(true) - $start_time) * 1000, 2).'ms');

		return $response;
	}

	/**
	 * @param URL|string $url
	 * @param Model $model
	 * @return bool|Model
	 */
	public function get($url, Model $model)
	{
		return $this->sendAPIRequest($url, Method::GET, $model);
	}

	/**
	 * @param URL|string $url
	 * @param Model $model
	 * @return bool|Model
	 */
	public function put($url, Model $model)
	{
		return $this->sendAPIRequest($url, Method::PUT, $model);
	}

	/**
	 * @param URL|string $url
	 * @param Model $model
	 * @return bool|Model
	 */
	public function post($url, Model $model)
	{
		return $this->sendAPIRequest($url, Method::POST, $model);
	}

	/**
	 * @param URL|string $url
	 * @return bool|Model
	 */
	public function delete($url)
	{
		return $this->sendAPIRequest($url, Method::DELETE);
	}

	/**
	 * @param URL|string $url
	 * @param string $method
	 * @param Model|null $data_model
	 * @param string $content_type
	 * @return Model|boolean
	 * @throws \Exception
	 */
	protected function sendAPIRequest($url, $method = Method::GET, $data_model = null, $content_type = ContentType::APPLICATION_JSON)
	{
		if (!is_object($url))
			$url = URL::Alloc()->initWithString($url);

		// Set up request
		$request = Request::Object();
		$request->setURL($url);
		$request->setMethod($method);
		$request->setAccept($content_type);
		$request->setHeader('X-Powered-By', $this->powered_by);
		$request->setHeader('X-Version', $this->version);

		// Set data
		if ($data_model && in_array($method, array(Method::PUT, Method::POST)))
		{
			if (is_subclass_of($data_model, '\SwampyPHP\Model\Image'))
				$content_type = ContentType::IMAGE;

			$request->setData($data_model, $content_type);
		}

		// Send request
		$response = $this->sendRequest($request);

		// If Unauthorized
		if ($response->getStatusCode() == StatusCode::UNAUTHORISED)
		{
			$auth = $response->getWWWAuthenticate();

			if (!$auth)
				throw new \Exception('Invalid response header:WWW-WAuthenticate', StatusCode::EXPECTATION_FAILED);

			// Process Authentication Challenge
			$credentials = $this->authentication_challenge_callback ? call_user_func($this->authentication_challenge_callback, $auth) : $this->handleAuthenticationChallenge($auth);

			if (!$credentials)
				throw new Exception(StatusCode::UNAUTHORISED);

			$auth->username = $credentials->username;
			$auth->password = $credentials->password;

			$auth->uri = $url->path;
			$auth->method = $method;
			$request->setAuthorization($auth);
			$response = $this->sendRequest($request);
		}

		$status_code = $response->getStatusCode();

		// Validate empty get response
		if ($method == Method::GET && $status_code ==  StatusCode::NO_CONTENT)
			return true;

		// Validate PUT & DELETE Methods
		if (in_array($method, array(Method::PUT, Method::DELETE, Method::HEAD, Method::OPTIONS)) && $status_code == StatusCode::OK)
			return true;

		$data = $response->getData();

		// Get data
		if (!$data || !is_array($data))
			throw new \Exception('Invalid response data', StatusCode::EXPECTATION_FAILED);
		elseif (array_key_exists('model', $data) && $data['model'] == 'Shopacitor\Model\Error')
		{
			$error = Error::ModelFromArray($data); //FIXME: I need fixing senpai t(-.-t)
			throw new \Exception($error->message, $error->code);
		}

		$data_model->setProperties($data); //FIXME: set me to setVars() maybe?

		return $data_model;
	}
}
